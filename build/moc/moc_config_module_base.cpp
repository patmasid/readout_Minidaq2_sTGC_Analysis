/****************************************************************************
** Meta object code from reading C++ file 'config_module_base.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/config_module_base.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'config_module_base.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Config_Base_t {
    QByteArrayData data[34];
    char stringdata0[591];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Config_Base_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Config_Base_t qt_meta_stringdata_Config_Base = {
    {
QT_MOC_LITERAL(0, 0, 11), // "Config_Base"
QT_MOC_LITERAL(1, 12, 21), // "mon_TTC_start_bit_val"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 20), // "mon_TTC_pulse_lv_val"
QT_MOC_LITERAL(4, 56, 16), // "mon_Gate_nBC_val"
QT_MOC_LITERAL(5, 73, 18), // "mon_L0_Latency_nBC"
QT_MOC_LITERAL(6, 92, 18), // "mon_L1_Latency_nBC"
QT_MOC_LITERAL(7, 111, 17), // "mon_L1_Accept_val"
QT_MOC_LITERAL(8, 129, 16), // "mon_BC_Reset_val"
QT_MOC_LITERAL(9, 146, 19), // "mon_Orbit_Reset_val"
QT_MOC_LITERAL(10, 166, 22), // "mon_VMM_Soft_Reset_val"
QT_MOC_LITERAL(11, 189, 17), // "mon_L0_Accept_val"
QT_MOC_LITERAL(12, 207, 14), // "mon_VMM_TP_val"
QT_MOC_LITERAL(13, 222, 17), // "mon_SCA_Reset_val"
QT_MOC_LITERAL(14, 240, 18), // "mon_EVID_Reset_val"
QT_MOC_LITERAL(15, 259, 20), // "mon_L0_Evt_Reset_val"
QT_MOC_LITERAL(16, 280, 17), // "mon_CKTP_NMax_val"
QT_MOC_LITERAL(17, 298, 20), // "mon_CKTP_Spacing_val"
QT_MOC_LITERAL(18, 319, 22), // "mon_Ext_Trigger_En_val"
QT_MOC_LITERAL(19, 342, 14), // "mon_Daq_En_val"
QT_MOC_LITERAL(20, 357, 16), // "mon_eFIFO_En_val"
QT_MOC_LITERAL(21, 374, 20), // "mon_TTC_phase_G0_val"
QT_MOC_LITERAL(22, 395, 20), // "mon_TTC_phase_G1_val"
QT_MOC_LITERAL(23, 416, 17), // "SCAInitSuccessful"
QT_MOC_LITERAL(24, 434, 8), // "uint32_t"
QT_MOC_LITERAL(25, 443, 7), // "boardID"
QT_MOC_LITERAL(26, 451, 20), // "ClearBoardMonitoring"
QT_MOC_LITERAL(27, 472, 22), // "Set_Board_SCA_GPIO_dir"
QT_MOC_LITERAL(28, 495, 23), // "Set_Board_SCA_GPIO_dout"
QT_MOC_LITERAL(29, 519, 18), // "query_TDS_register"
QT_MOC_LITERAL(30, 538, 4), // "itds"
QT_MOC_LITERAL(31, 543, 18), // "query_ROC_register"
QT_MOC_LITERAL(32, 562, 11), // "analog_digi"
QT_MOC_LITERAL(33, 574, 16) // "configSuccessful"

    },
    "Config_Base\0mon_TTC_start_bit_val\0\0"
    "mon_TTC_pulse_lv_val\0mon_Gate_nBC_val\0"
    "mon_L0_Latency_nBC\0mon_L1_Latency_nBC\0"
    "mon_L1_Accept_val\0mon_BC_Reset_val\0"
    "mon_Orbit_Reset_val\0mon_VMM_Soft_Reset_val\0"
    "mon_L0_Accept_val\0mon_VMM_TP_val\0"
    "mon_SCA_Reset_val\0mon_EVID_Reset_val\0"
    "mon_L0_Evt_Reset_val\0mon_CKTP_NMax_val\0"
    "mon_CKTP_Spacing_val\0mon_Ext_Trigger_En_val\0"
    "mon_Daq_En_val\0mon_eFIFO_En_val\0"
    "mon_TTC_phase_G0_val\0mon_TTC_phase_G1_val\0"
    "SCAInitSuccessful\0uint32_t\0boardID\0"
    "ClearBoardMonitoring\0Set_Board_SCA_GPIO_dir\0"
    "Set_Board_SCA_GPIO_dout\0query_TDS_register\0"
    "itds\0query_ROC_register\0analog_digi\0"
    "configSuccessful"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Config_Base[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      28,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  154,    2, 0x06 /* Public */,
       3,    0,  155,    2, 0x06 /* Public */,
       4,    0,  156,    2, 0x06 /* Public */,
       5,    0,  157,    2, 0x06 /* Public */,
       6,    0,  158,    2, 0x06 /* Public */,
       7,    0,  159,    2, 0x06 /* Public */,
       8,    0,  160,    2, 0x06 /* Public */,
       9,    0,  161,    2, 0x06 /* Public */,
      10,    0,  162,    2, 0x06 /* Public */,
      11,    0,  163,    2, 0x06 /* Public */,
      12,    0,  164,    2, 0x06 /* Public */,
      13,    0,  165,    2, 0x06 /* Public */,
      14,    0,  166,    2, 0x06 /* Public */,
      15,    0,  167,    2, 0x06 /* Public */,
      16,    0,  168,    2, 0x06 /* Public */,
      17,    0,  169,    2, 0x06 /* Public */,
      18,    0,  170,    2, 0x06 /* Public */,
      19,    0,  171,    2, 0x06 /* Public */,
      20,    0,  172,    2, 0x06 /* Public */,
      21,    0,  173,    2, 0x06 /* Public */,
      22,    0,  174,    2, 0x06 /* Public */,
      23,    1,  175,    2, 0x06 /* Public */,
      26,    1,  178,    2, 0x06 /* Public */,
      27,    1,  181,    2, 0x06 /* Public */,
      28,    1,  184,    2, 0x06 /* Public */,
      29,    2,  187,    2, 0x06 /* Public */,
      31,    2,  192,    2, 0x06 /* Public */,
      33,    1,  197,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 24, 0x80000000 | 24,   25,   30,
    QMetaType::Void, 0x80000000 | 24, 0x80000000 | 24,   25,   32,
    QMetaType::Void, 0x80000000 | 24,   25,

       0        // eod
};

void Config_Base::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Config_Base *_t = static_cast<Config_Base *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->mon_TTC_start_bit_val(); break;
        case 1: _t->mon_TTC_pulse_lv_val(); break;
        case 2: _t->mon_Gate_nBC_val(); break;
        case 3: _t->mon_L0_Latency_nBC(); break;
        case 4: _t->mon_L1_Latency_nBC(); break;
        case 5: _t->mon_L1_Accept_val(); break;
        case 6: _t->mon_BC_Reset_val(); break;
        case 7: _t->mon_Orbit_Reset_val(); break;
        case 8: _t->mon_VMM_Soft_Reset_val(); break;
        case 9: _t->mon_L0_Accept_val(); break;
        case 10: _t->mon_VMM_TP_val(); break;
        case 11: _t->mon_SCA_Reset_val(); break;
        case 12: _t->mon_EVID_Reset_val(); break;
        case 13: _t->mon_L0_Evt_Reset_val(); break;
        case 14: _t->mon_CKTP_NMax_val(); break;
        case 15: _t->mon_CKTP_Spacing_val(); break;
        case 16: _t->mon_Ext_Trigger_En_val(); break;
        case 17: _t->mon_Daq_En_val(); break;
        case 18: _t->mon_eFIFO_En_val(); break;
        case 19: _t->mon_TTC_phase_G0_val(); break;
        case 20: _t->mon_TTC_phase_G1_val(); break;
        case 21: _t->SCAInitSuccessful((*reinterpret_cast< uint32_t(*)>(_a[1]))); break;
        case 22: _t->ClearBoardMonitoring((*reinterpret_cast< uint32_t(*)>(_a[1]))); break;
        case 23: _t->Set_Board_SCA_GPIO_dir((*reinterpret_cast< uint32_t(*)>(_a[1]))); break;
        case 24: _t->Set_Board_SCA_GPIO_dout((*reinterpret_cast< uint32_t(*)>(_a[1]))); break;
        case 25: _t->query_TDS_register((*reinterpret_cast< uint32_t(*)>(_a[1])),(*reinterpret_cast< uint32_t(*)>(_a[2]))); break;
        case 26: _t->query_ROC_register((*reinterpret_cast< uint32_t(*)>(_a[1])),(*reinterpret_cast< uint32_t(*)>(_a[2]))); break;
        case 27: _t->configSuccessful((*reinterpret_cast< uint32_t(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_TTC_start_bit_val)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_TTC_pulse_lv_val)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_Gate_nBC_val)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_L0_Latency_nBC)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_L1_Latency_nBC)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_L1_Accept_val)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_BC_Reset_val)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_Orbit_Reset_val)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_VMM_Soft_Reset_val)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_L0_Accept_val)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_VMM_TP_val)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_SCA_Reset_val)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_EVID_Reset_val)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_L0_Evt_Reset_val)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_CKTP_NMax_val)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_CKTP_Spacing_val)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_Ext_Trigger_En_val)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_Daq_En_val)) {
                *result = 17;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_eFIFO_En_val)) {
                *result = 18;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_TTC_phase_G0_val)) {
                *result = 19;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::mon_TTC_phase_G1_val)) {
                *result = 20;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)(uint32_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::SCAInitSuccessful)) {
                *result = 21;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)(uint32_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::ClearBoardMonitoring)) {
                *result = 22;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)(uint32_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::Set_Board_SCA_GPIO_dir)) {
                *result = 23;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)(uint32_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::Set_Board_SCA_GPIO_dout)) {
                *result = 24;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)(uint32_t , uint32_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::query_TDS_register)) {
                *result = 25;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)(uint32_t , uint32_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::query_ROC_register)) {
                *result = 26;
                return;
            }
        }
        {
            typedef void (Config_Base::*_t)(uint32_t );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Config_Base::configSuccessful)) {
                *result = 27;
                return;
            }
        }
    }
}

const QMetaObject Config_Base::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Config_Base.data,
      qt_meta_data_Config_Base,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Config_Base::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Config_Base::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Config_Base.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Config_Base::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 28)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 28;
    }
    return _id;
}

// SIGNAL 0
void Config_Base::mon_TTC_start_bit_val()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Config_Base::mon_TTC_pulse_lv_val()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void Config_Base::mon_Gate_nBC_val()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void Config_Base::mon_L0_Latency_nBC()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void Config_Base::mon_L1_Latency_nBC()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void Config_Base::mon_L1_Accept_val()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void Config_Base::mon_BC_Reset_val()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void Config_Base::mon_Orbit_Reset_val()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void Config_Base::mon_VMM_Soft_Reset_val()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void Config_Base::mon_L0_Accept_val()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void Config_Base::mon_VMM_TP_val()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void Config_Base::mon_SCA_Reset_val()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void Config_Base::mon_EVID_Reset_val()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void Config_Base::mon_L0_Evt_Reset_val()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void Config_Base::mon_CKTP_NMax_val()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}

// SIGNAL 15
void Config_Base::mon_CKTP_Spacing_val()
{
    QMetaObject::activate(this, &staticMetaObject, 15, nullptr);
}

// SIGNAL 16
void Config_Base::mon_Ext_Trigger_En_val()
{
    QMetaObject::activate(this, &staticMetaObject, 16, nullptr);
}

// SIGNAL 17
void Config_Base::mon_Daq_En_val()
{
    QMetaObject::activate(this, &staticMetaObject, 17, nullptr);
}

// SIGNAL 18
void Config_Base::mon_eFIFO_En_val()
{
    QMetaObject::activate(this, &staticMetaObject, 18, nullptr);
}

// SIGNAL 19
void Config_Base::mon_TTC_phase_G0_val()
{
    QMetaObject::activate(this, &staticMetaObject, 19, nullptr);
}

// SIGNAL 20
void Config_Base::mon_TTC_phase_G1_val()
{
    QMetaObject::activate(this, &staticMetaObject, 20, nullptr);
}

// SIGNAL 21
void Config_Base::SCAInitSuccessful(uint32_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}

// SIGNAL 22
void Config_Base::ClearBoardMonitoring(uint32_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 22, _a);
}

// SIGNAL 23
void Config_Base::Set_Board_SCA_GPIO_dir(uint32_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 23, _a);
}

// SIGNAL 24
void Config_Base::Set_Board_SCA_GPIO_dout(uint32_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 24, _a);
}

// SIGNAL 25
void Config_Base::query_TDS_register(uint32_t _t1, uint32_t _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 25, _a);
}

// SIGNAL 26
void Config_Base::query_ROC_register(uint32_t _t1, uint32_t _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 26, _a);
}

// SIGNAL 27
void Config_Base::configSuccessful(uint32_t _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 27, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
