#
# project file for stgc-dcs
#
# ssun@cern.ch
# Jan 2018
#

#####################################################
# directory and machine specific items here
#####################################################
linebreak="---------------------------------------------------------------"

# for now, the user specifies where Boost is located
boostinclude=/opt/Boost/include/
boostlib=/opt/Boost/lib/

# do not touch these
sourcepath=../src
includepath=../include
imagepath=../images

message($$linebreak)
message("qmake sourcepath:      $$sourcepath")
message("qmake includepath:     $$includepath")
message("qmake boostinclude:    $$boostinclude")
message("qmake boostlib:        $$boostlib")
message("qmake imagepath:       $$imagepath")
message("ROOTSYS:               $(ROOTSYS)")
message($$linebreak)

#####################################################

QT      += core gui
QT      += network
QT      += widgets
QT      += xml
CONFIG  += console
CONFIG  +=declarative_debug
#CONFIG  +=c++11
CONFIG  +=c++1y

TARGET   = stgc-dcs
TEMPLATE = app

# make sure the boost version we point to is this one
DEFINES += BOOST_ALL_NO_LIB

linux {
	QMAKE_RPATHDIR += $$boostlib
	QMAKE_RPATHDIR += ./objects
}

INCLUDEPATH += $(ROOTSYS)/include
win32:LIBS += -L$(ROOTSYS)/lib -llibCint -llibRIO -llibNet \
       -llibHist -llibGraf -llibGraf3d -llibGpad -llibTree \
       -llibRint -llibPostscript -llibMatrix -llibPhysics \
       -llibGui -llibRGL -llibMathCore
else:LIBS += -L$(ROOTSYS)/lib -lCore -lRIO -lNet \
       -lHist -lGraf -lGraf3d -lGpad -lTree \
       -lRint -lPostscript -lMatrix -lPhysics \
       -lGui -lMathCore #-lRGL -lMathCore

LIBS +=  -L$$boostlib -lboost_thread-mt -lboost_filesystem-mt  -lboost_system-mt -lboost_chrono-mt -lboost_atomic-mt

LIBS += -L./objects

INCLUDEPATH += $$includepath
DEPENDPATH  += $$includepath
INCLUDEPATH += $$includepath/configuration/
DEPENDPATH  += $$includepath/configuration/
INCLUDEPATH += $$boostinclude
DEPENDPATH  += $$boostinclude

OBJECTS_DIR += ./objects/
MOC_DIR     += ./moc/
RCC_DIR     += ./rcc/
UI_DIR      += ./ui/

SOURCES += $$sourcepath/main.cxx\
           ## config handler
           $$sourcepath/config_handler_miniDAQ.cxx\
           ## configuration
           $$sourcepath/configuration/miniDAQ_global_setting.cxx\
           $$sourcepath/configuration/miniDAQ_ttc_setting.cxx\
           $$sourcepath/configuration/Board_Info.cxx\
           ## other tools
           $$sourcepath/message_handler.cxx\
           ## Analysing Histos
           $$sourcepath/Anal_Minidaq2_readout.cxx\
           $$sourcepath/Anal_NTuples.cxx\
           $$sourcepath/Ana_Minidaq2_Summary.cxx

HEADERS  += $$includepath/configuration/miniDAQ_global_setting.h\
            $$includepath/configuration/miniDAQ_ttc_setting.h\
            $$includepath/configuration/Board_Info.h\
            $$includepath/config_handler_miniDAQ.h\
            $$includepath/message_handler.h\
            $$includepath/Anal_Minidaq2_readout.h\
            $$includepath/Anal_NTuples.h\
            $$includepath/Ana_Minidaq2_Summary.h

FORMS    += $$sourcepath/mainwindow.ui

RESOURCES += \
    $$imagepath/icons.qrc
