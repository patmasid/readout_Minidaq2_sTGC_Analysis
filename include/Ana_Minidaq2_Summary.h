#ifndef ANA_MINIDAQ2_SUMMARY_H
#define ANA_MINIDAQ2_SUMMARY_H

// qt                                                                                                      
#include <QApplication>
#include <QObject>
#include <QList>
#include <QStringList>
#include <QHostAddress>

// boost                                                                                                                            
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// std/stdl                                                                                                                                                   
#include <iostream>
#include <fstream>
#include <sstream>

// vmm                                                                                                                                                                        
#include <string>

#include <iostream>
#include <sstream>
#include <fstream>

#include <iterator>

#include <vector>
#include <utility>

#include <algorithm>

#include "TString.h"

#include <cmath>
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <TCanvas.h>

#include "TLegend.h"
#include "TStyle.h"
#include "THStack.h"

#include "TROOT.h"
#include "TKey.h"

#include "Anal_Minidaq2_readout.h"
#include "Anal_NTuples.h"
#include "Board_Info.h"

class Ana_Minidaq2_Summary{

 public :

  Ana_Minidaq2_Summary( std::vector<QString> *run_names, std::vector<QString> *run_dir);
  virtual ~Ana_Minidaq2_Summary();

  bool verbose;

  std::vector<Anal_Minidaq2_readout*> *run_analyzers;
  std::vector<Anal_NTuples*>          *run_ntuples;

  std::vector<Board_Info*> *Board_Informations_Combined;
  
  void Init(std::vector<QString> *run_names, std::vector<QString> *run_dir);

  void Clear();

  void SetVerbose(bool v);

  void RunAnalysis();
  void CombineAnalyzedData();

  void PrintSummaryFile(QString outfile_txt);
  
  
  std::ofstream m_strOutput;

};

#endif
