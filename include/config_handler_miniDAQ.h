#ifndef CONFIG_HANDLER_MINIDAQ_H
#define CONFIG_HANDLER_MINIDAQ_H

/////////////////////////////////////////
//
// config_handler
//
// containers for configuration of KC705 miniDAQ
//
// Siyuan.Sun@cern.ch
// August 2017
//
//////////////////////////////////////////

// qt
#include <QObject>
#include <QList>
#include <QStringList>
#include <QHostAddress>

// boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// std/stdl
#include <iostream>

// vmm
#include "message_handler.h"

#include "miniDAQ_global_setting.h"
#include "miniDAQ_ttc_setting.h"

// ---------------------------------------------------------------------- //
//  Main Configuration Handler tool
// ---------------------------------------------------------------------- //
class ConfigHandlerMiniDAQ : public QObject
{
    Q_OBJECT

    public :
        explicit ConfigHandlerMiniDAQ(QObject *parent = 0);
        virtual ~ConfigHandlerMiniDAQ(){};

        ConfigHandlerMiniDAQ& setDebug(bool dbg) { m_dbg = dbg; return *this;}
        bool dbg() { return m_dbg; }

        void LoadMessageHandler(MessageHandler& msg);
        MessageHandler& msg() { return *m_msg; }


	std::vector<std::vector<int>> CheckLinkLocked( std::vector< std::vector< std::vector<bool> >> LinkOffsets,
						       int & nlinks );

	bool All_Boards_Configed() { return ( (  m_global_miniDAQ.board_exist_mask      & 0b11111111 ) &
					      ~( m_global_miniDAQ.configured_board_mask & 0b11111111 ) ) == 0; }

	bool AllBoardElinksDetected();

	int  AllBoards_TTCAligned();
	
	int nElinks();

	int          TargetPort();
	QHostAddress TargetIp();

	bool LoadMiniDAQConfig_from_File(const QString &filename);
	void WriteMiniDAQConfig_to_File(       QString filename);

	MiniDAQGlobalSetting LoadMiniDAQGlobalSettings(const boost::property_tree::ptree& p);
	MiniDAQTTCSetting LoadMiniDAQTTCSettings(      const boost::property_tree::ptree& p);

        // methods for GUI interaction
	//void LoadMiniDAQConfiguration( MiniDAQ_Setting &miniDAQ );
	void LoadMiniDAQConfiguration(      MiniDAQGlobalSetting &global, MiniDAQTTCSetting &ttc );
	void LoadMiniDAQConfigurationGlobal(MiniDAQGlobalSetting &global );
	void LoadMiniDAQConfigurationTTC(   MiniDAQTTCSetting    &ttc    );

	void clear_miniDAQ_global();
	void clear_miniDAQ_ttc();

	void cannotDetectBoard(uint32_t i);

        // retrieve the objects

        MiniDAQGlobalSetting& miniDAQ_Global_Settings() { return m_global_miniDAQ; }
        MiniDAQTTCSetting&    miniDAQ_TTC_Settings()    { return m_ttc_miniDAQ; }
	
    private :
        bool m_dbg;

	std::stringstream m_sx;

        MiniDAQGlobalSetting    m_global_miniDAQ;
        MiniDAQTTCSetting       m_ttc_miniDAQ;

        MessageHandler* m_msg;

 signals :

	void new_ttc_config();
	void new_global_config();

	

}; // class ConfigHandlerMiniDAQ



#endif
