#ifndef ANAL_MINIDAQ2_READOUT_H
#define ANAL_MINIDAQ2_READOUT_H

// qt                                                                                                      
#include <QApplication>
#include <QObject>
#include <QList>
#include <QStringList>
#include <QHostAddress>

// boost                                                                                                                            
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

// std/stdl                                                                                                                                                   
#include <iostream>
#include <fstream>
#include <sstream>

// vmm                                                                                                                                                                        
#include "message_handler.h"
#include "miniDAQ_global_setting.h"
#include "miniDAQ_ttc_setting.h"

#include <string>

#include <iostream>
#include <sstream>
#include <fstream>

#include <iterator>

#include <vector>
#include <utility>

#include <algorithm>

#include "TString.h"

#include <cmath>
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <TCanvas.h>

#include "TLegend.h"
#include "TStyle.h"
#include "THStack.h"

#include "TROOT.h"
#include "TKey.h"

#include "config_handler_miniDAQ.h"
#include "Board_Info.h"

class Anal_Minidaq2_readout{

 public :

  Anal_Minidaq2_readout( QString inputXml_, QString inputHistosRoot_);
  virtual ~Anal_Minidaq2_readout();

  bool verbose;

  QString inputXml;
  QString inputHistosRoot;
  
  void Init();

  void Clear();

  void SetVerbose(bool v);

  std::ofstream m_strOutput;

  TFile *_inputHistfile;

  std::vector<Board_Info*> * Board_Informations;
  
  std::vector<uint32_t> VMMs_Configured_mask;
  std::vector<bool> v_boardsChecked;
  std::vector< std::vector< int > > max_binContent;
  
  std::string ChannelRange;

  int BoardSize;
  std::vector<int> VMMsize;

  MiniDAQGlobalSetting m_global;

  ConfigHandlerMiniDAQ *m_ConfigHandlerMiniDAQ;
  
  bool GetInfoFromXml();
  bool GetInfoFromHistos();
  bool PrintReport_for8Boards();
  bool PrintReport_for8Boards_Sheet();


};

#endif
