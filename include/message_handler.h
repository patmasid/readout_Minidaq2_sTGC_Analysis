#ifndef MESSAGE_HANDLER_H
#define MESSAGE_HANDLER_H

/////////////////////////////////////////
//
// message_handler
//
// Tool for handling output of messages
// to the GUI message panel
//
//  - Currently not thread-safe and
//    is relatively unsophisticated
//
// daniel.joseph.antrim@cern.ch
// March 2016
//
//////////////////////////////////////////

// qt
#include <QObject>
#include <QString>
#include <QTime>
#include <QDate>

// std/stl
#include <iostream>
#include <sstream>
#include <fstream>

/////////////////////////////////////////////////////////////////////////////
// ----------------------------------------------------------------------- //
//  MessageHandler
// ----------------------------------------------------------------------- //
/////////////////////////////////////////////////////////////////////////////

class MessageHandler : public QObject
{
    Q_OBJECT;

    public :
        explicit MessageHandler(QObject *parent = 0);
        virtual ~MessageHandler();
        void setGUI(bool set) { m_gui = set; }
        bool gui() { return m_gui; }
        void setMessageSize(unsigned int size) { m_size = size; }
	void OpenLog( QString log_name );

        std::string buffer() { return m_buffer.str(); }
        void clear() { m_buffer.str(""); }

        /// operators
        void operator () (std::stringstream& s, std::string caller,
                                                    bool exit = false);

        void operator () (std::stringstream& s, const char* caller,
                                                        bool exit = false);

        void operator () (std::stringstream& s, bool exit = false);

        void operator () (std::string s, std::string caller,
                                                    bool exit = false);

        void operator () (std::string s, const char* caller,
                                                    bool exit = false);

        void operator () (const char* m, const char* caller, bool exit = false);

        void operator () (std::string s, bool exit = false);

        void operator () (const char* msg, bool exit = false);

        void standard_message(std::string s);

	QString get_log_name() { return m_log_name; }

    private :
        bool m_gui;
        unsigned int m_size;
        int m_callborder;

	QString m_log_name;
	
	std::ofstream log_file;

	QString DateStr();
        QString TimeStr();

        std::stringstream m_buffer;

 signals :
        void logReady();

    public slots :

};

#endif
