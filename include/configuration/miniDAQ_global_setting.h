#ifndef MINIDAQ_GLOBAL_SETTING_H
#define MINIDAQ_GLOBAL_SETTING_H

#include <QStringList>
#include <QList>

class MiniDAQGlobalSetting {

    public :
        MiniDAQGlobalSetting();
        virtual ~MiniDAQGlobalSetting(){};

	QString  target_ip;
	int target_port;

	std::vector<uint32_t> elink_locked_mask;
	uint32_t board_exist_mask;

	// < phase, board, elink >
	std::vector<std::vector<std::vector< bool >>> elink_matrix;

	int configured_board_mask;

	std::vector<uint32_t> board_SCA_IDs;
	std::vector<bool> board_is_pFEB;

	std::vector<uint32_t> board_GPIO_dir_mask;
	std::vector<uint32_t> board_GPIO_dout_mask;

	std::vector<uint32_t> board_ROC_configed_mask;
	std::vector<uint32_t> board_VMM_configed_mask;
	std::vector<uint32_t> board_TDS_configed_mask;

	std::vector<std::vector<std::vector<uint32_t>>> board_current_TDS_register;
	std::vector<std::vector<std::vector<uint32_t>>> board_current_ROC_register;

	std::vector<int> m_aligned_TTC_starting_bits;
	
	void setElinkLockedVector( std::vector<std::vector<bool>> locked_elinks );
	uint32_t ComputeElinkMask();

        void set_board_SCA_IDs( std::vector<uint32_t> SCA_IDs ) { board_SCA_IDs = SCA_IDs; }
        void set_board_exist_mask( uint32_t exist_mask )        { board_exist_mask = exist_mask; }

	std::vector<uint32_t> board_TTC_phase;

	void clear();
        void validate();
        void print();
        bool ok; // loading went ok

};

#endif
