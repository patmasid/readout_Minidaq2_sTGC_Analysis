#ifndef BOARD_INFO_H
#define BOARD_INFO_H

#include<QString>
#include<vector>
#include<utility>

class Board_Info {

    public :
        Board_Info();
        virtual ~Board_Info();
	
	uint32_t SCA_ID;
	bool is_pFEB;

	std::vector<bool> *VMMs_Configured;
	std::vector<bool> *v_boardsChecked;
	std::vector<int>  *max_binContent;
	std::vector<int>  *hist_Found;

	std::vector< std::vector<int>* > *v_dead_channels;
	std::vector< std::vector<std::pair<int,double>>* > *v_lowpdoChannels;
	std::vector< std::vector<std::pair<int,double>>* > *v_highpdoChannels;
	std::vector< std::vector<std::pair<int,double>>* > *v_lessHitsChannels;

	bool Add_New_Configed_VMM(    uint ivmm );
	bool Add_New_Found_VMM_Histo( uint ivmm );
	bool Add_New_Dead_Channel(    uint ivmm, int ichan );
	bool Add_New_Ineff_Channel(   uint ivmm, int ichan, double eff );
	bool Add_New_LowPDOChannel(   uint ivmm, int ichan, double i_avg_pdo );
	bool Add_New_HighPDOChannel(  uint ivmm, int ichan, double i_avg_pdo );

	void Sorted_Insert( std::vector<int> * vec, int val );
	void Sorted_Insert( std::vector<uint32_t> * vec, uint32_t val );
	void Sorted_Insert( std::vector< std::pair<int,double> > * vec, std::pair<int,double> val );
	
	bool UploadInfo( Board_Info *new_info ); 
	void Clear();
};

#endif
