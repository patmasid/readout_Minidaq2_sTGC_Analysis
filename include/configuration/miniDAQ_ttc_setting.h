#ifndef MINIDAQ_TTC_SETTING_H
#define MINIDAQ_TTC_SETTING_H

#include <QStringList>
#include <QList>

class MiniDAQTTCSetting {

    public :
        MiniDAQTTCSetting();
        virtual ~MiniDAQTTCSetting(){};

        uint32_t TTC_start_bit;
        uint32_t TTC_pulse_lv_val;
        uint32_t Gate_nBC;
        uint32_t L0_Latency_nBC;
        uint32_t L1_Latency_nBC;
       
        int L1_Accept_val;
        int BC_Reset_val;
	int Orbit_Reset_val;
        int VMM_Soft_Reset_val;
        int L0_Accept_val;
        int VMM_TP_val;
        int SCA_Reset_val;
        int EVID_Reset_val;
        int L0_Evt_Reset_val;

        uint32_t CKTP_NMax;
        uint32_t CKTP_Spacing;

        int Ext_Trigger_En;
        int Daq_En;

        uint32_t eFIFO_value;
 
	void clear();
        void print();
        bool ok; // loading went ok

};

#endif
