//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Sep  3 08:49:22 2019 by ROOT version 6.14/04
// from TTree decoded_event_tree/decoded_event_tree
// found on file: run_test2_nEvt_100_decoded.root
//////////////////////////////////////////////////////////

#ifndef Anal_NTuples_h
#define Anal_NTuples_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include "TClonesArray.h"
#include "TObject.h"

// Header file for the classes stored in the TTree if any.
#include "vector"

#include <iostream>
#include <fstream>
#include <cmath>
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TLorentzVector.h"
#include <vector>
#include "TGraph.h"
#include <fstream>
#include "Anal_Minidaq2_readout.h"
#include "TString.h"

using namespace std;

class Anal_NTuples {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          m_l1_id;
   Int_t           m_nhits;
   Int_t           m_nNullPackets;
   vector<unsigned int> *m_tdo;
   vector<unsigned int> *m_pdo;
   vector<unsigned int> *m_chan;
   vector<unsigned int> *m_vmm_id;
   vector<unsigned int> *m_elink_id;
   vector<unsigned int> *m_bcid_rel;
   vector<unsigned int> *m_bcid;
   vector<unsigned int> *m_neighbour;
   vector<unsigned int> *m_parity;
   vector<unsigned int> *m_orbit;
   vector<unsigned int> *m_l0_id;
   vector<unsigned int> *m_null_bcid;
   vector<unsigned int> *m_null_elink_id;
   vector<unsigned int> *m_null_orbit;
   vector<unsigned int> *m_null_l1_id;
   vector<unsigned int> *m_null_l0_id;

   // List of branches
   TBranch        *b_m_l1_id;   //!
   TBranch        *b_m_nhits;   //!
   TBranch        *b_m_nNullPackets;   //!
   TBranch        *b_m_tdo;   //!
   TBranch        *b_m_pdo;   //!
   TBranch        *b_m_chan;   //!
   TBranch        *b_m_vmm_id;   //!
   TBranch        *b_m_elink_id;   //!
   TBranch        *b_m_bcid_rel;   //!
   TBranch        *b_m_bcid;   //!
   TBranch        *b_m_neighbour;   //!
   TBranch        *b_m_parity;   //!
   TBranch        *b_m_orbit;   //!
   TBranch        *b_m_l0_id;   //!
   TBranch        *b_m_null_bcid;   //!
   TBranch        *b_m_null_elink_id;   //!
   TBranch        *b_m_null_orbit;   //!
   TBranch        *b_m_null_l1_id;   //!
   TBranch        *b_m_null_l0_id;   //!

   QString inputXml;
   QString inputFileList;
   std::string outFileName;
   
   Anal_NTuples( QString inputXml_, QString inputFileList_, string outFileName_);
   ~Anal_NTuples();

   Int_t    Cut(Long64_t entry);
   Int_t    GetEntry(Long64_t entry);
   Long64_t LoadTree(Long64_t entry);
   void     Init(TTree *tree);
   Bool_t FillChain(TChain *chain);
   virtual void     Loop();
   Bool_t   Notify();
   void     Show(Long64_t entry = -1);
   
   void InitMapHistograms(Anal_Minidaq2_readout* read_obj);
   void CreateHistDir();
   void End();
   
   Anal_Minidaq2_readout *analMinidaq2Obj;

   std::vector<std::string> DirName_Boards;
   std::vector< std::vector<std::string> > SubDirName1_Boards;
   std::vector< std::vector<std::string> > SubDirName2_Boards;

   std::vector<unsigned int> SCA_ID_Boards;
   std::vector< std::vector<bool> > VMMs_Configured;

   int BoardSize;
   std::vector<int> VMMsize;
   
   vector<vector<TH2F*>> h_Channel_Vs_level1Id;
   vector<vector<TH2F*>> h_Pdo_Vs_Channel;

   TFile *oFile;

};

#endif

//#ifdef Anal_NTuples_cxx

//#endif // #ifdef Anal_NTuples_cxx





