source install.sh

(In build directory)
make 2>&1 |tee stgc_dcs_compile.log

(Two arguments to ./stgc-dcs)
./stgc-dcs <path to minidaq_current_tmp.xml file> <path to the decoded.root file>

(output you get is)
*TABLE.txt (Containing the outputs in a table format. You can copy this text and paste it in excel of google spreadsheet with 'space' as a delimiter.)
*Hists.root (Contains Channel_Vs_Level1Id and Pdo_Vs_Channel Plots for the connected boards)

Note: 

in main.cxx: if you use 'ana.PrintReport_for8Boards' function, it creates a *REPORT.txt file for the report.
             if	you use	'ana.PrintReport_for8Boards_Sheet' function, it creates a *TABLE.txt file in TABLE format for the report mentioned above. 