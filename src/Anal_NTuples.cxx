#define Anal_NTuples_cxx
#include "Anal_NTuples.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

#include <iostream>
#include <vector>
#include <cstring>
#include "../include/Anal_Minidaq2_readout.h"
#include "../include/Anal_NTuples.h"
#include <cmath>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <algorithm>

#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <array>

#include <iomanip>
#include "THStack.h"
#include <math.h>
#include <TLine.h>

#include "TApplication.h"
#include <TSystem.h>

#include <fstream>
#include <cstdint>

#include <sys/stat.h>

// if parameter tree is not specified (or zero), connect the file                                                                                                                                           
// used to generate this class and read the Tree.                                                                                                                                                           

Anal_NTuples::Anal_NTuples( QString inputXml_, QString inputFileList_, string outFileName_) {

  inputXml      = inputXml_;
  inputFileList = inputFileList_;
  outFileName   = outFileName_;

  TChain *tree = new TChain("decoded_event_tree");

  if( ! FillChain(tree) ) {
    std::cerr << "Cannot get the tree " << std::endl;
  } else {
    std::cout << "Initiating the analysis " << std::endl;
  }

  Init(tree);

  oFile = new TFile(outFileName.c_str(), "recreate");

}

Anal_NTuples::~Anal_NTuples()
{
  if (!fChain) return;
  delete fChain->GetCurrentFile();
}

Int_t Anal_NTuples::GetEntry(Long64_t entry)
{
  // Read contents of entry.                                                                                                                                                                                  
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t Anal_NTuples::LoadTree(Long64_t entry)
{
  // Set the environment to read one entry                                                                                                                                                                    
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
    Notify();
  }
  return centry;
}

void Anal_NTuples::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize                                                                                                                                   
  // a new tree or chain. Typically here the branch addresses and branch                                                                                                                                   
  // pointers of the tree will be set.                                                                                                                                                                     
  // It is normally not necessary to make changes to the generated                                                                                                                                         
  // code, but the routine can be extended by the user if needed.                                                                                                                                          
  // Init() will be called many times when running on PROOF                                                                                                                                                
  // (once per file to be processed).                                                                                                                                                                      

  // Set object pointer                                                                                                                                                                                    
  m_tdo = 0;
  m_pdo = 0;
  m_chan = 0;
  m_vmm_id = 0;
  m_elink_id = 0;
  m_bcid_rel = 0;
  m_bcid = 0;
  m_neighbour = 0;
  m_parity = 0;
  m_orbit = 0;
  m_l0_id = 0;
  m_null_bcid = 0;
  m_null_elink_id = 0;
  m_null_orbit = 0;
  m_null_l1_id = 0;
  m_null_l0_id = 0;
  // Set branch addresses and branch pointers                                                                                                                                                              
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("m_l1_id", &m_l1_id, &b_m_l1_id);
  fChain->SetBranchAddress("m_nhits", &m_nhits, &b_m_nhits);
  fChain->SetBranchAddress("m_nNullPackets", &m_nNullPackets, &b_m_nNullPackets);
  fChain->SetBranchAddress("m_tdo", &m_tdo, &b_m_tdo);
  fChain->SetBranchAddress("m_pdo", &m_pdo, &b_m_pdo);
  fChain->SetBranchAddress("m_chan", &m_chan, &b_m_chan);
  fChain->SetBranchAddress("m_vmm_id", &m_vmm_id, &b_m_vmm_id);
  fChain->SetBranchAddress("m_elink_id", &m_elink_id, &b_m_elink_id);
  fChain->SetBranchAddress("m_bcid_rel", &m_bcid_rel, &b_m_bcid_rel);
  fChain->SetBranchAddress("m_bcid", &m_bcid, &b_m_bcid);
  fChain->SetBranchAddress("m_neighbour", &m_neighbour, &b_m_neighbour);
  fChain->SetBranchAddress("m_parity", &m_parity, &b_m_parity);
  fChain->SetBranchAddress("m_orbit", &m_orbit, &b_m_orbit);
  fChain->SetBranchAddress("m_l0_id", &m_l0_id, &b_m_l0_id);
  fChain->SetBranchAddress("m_null_bcid", &m_null_bcid, &b_m_null_bcid);
  fChain->SetBranchAddress("m_null_elink_id", &m_null_elink_id, &b_m_null_elink_id);
  fChain->SetBranchAddress("m_null_orbit", &m_null_orbit, &b_m_null_orbit);
  fChain->SetBranchAddress("m_null_l1_id", &m_null_l1_id, &b_m_null_l1_id);
  fChain->SetBranchAddress("m_null_l0_id", &m_null_l0_id, &b_m_null_l0_id);
  Notify();
}

Bool_t Anal_NTuples::FillChain(TChain *chain) {

   std::cout << "TreeUtilities : FillChain from " << inputFileList.toStdString() << std::endl;
   chain->Add(inputFileList.toStdString().c_str());

  std::cout << "No. of Entries in this tree : " << chain->GetEntries() << std::endl;
  return kTRUE;
}

Bool_t Anal_NTuples::Notify()
{
  // The Notify() function is called when a new file is opened. This                                                                                                                                       
  // can be either for a new TTree in a TChain or when when a new TTree                                                                                                                                    
  // is started when using PROOF. It is normally not necessary to make changes                                                                                                                             
  // to the generated code, but the routine can be extended by the                                                                                                                                         
  // user if needed. The return value is currently not used.                                                                                                                                               

  return kTRUE;
}

void Anal_NTuples::Show(Long64_t entry)
{
  // Print contents of entry.                                                                                                                                                                                 
  // If entry is not specified, print current entry                                                                                                                                                           
  if (!fChain) return;
  fChain->Show(entry);
}

Int_t Anal_NTuples::Cut(Long64_t entry)
{
  // This function may be called from Loop.                                                                                                                                                                   
  // returns  1 if entry is accepted.                                                                                                                                                                         
  // returns -1 otherwise.                                                                                                                                                                                    
  return 1;
}

void Anal_NTuples::InitMapHistograms( Anal_Minidaq2_readout* readout_obj){

  analMinidaq2Obj = readout_obj;
  
  VMMsize = analMinidaq2Obj->VMMsize;

  DirName_Boards.resize(8);
  SubDirName1_Boards.resize(8);
  SubDirName2_Boards.resize(8);
  h_Channel_Vs_level1Id.resize(8);
  h_Pdo_Vs_Channel.resize(8);
  for(int ii=0; ii<8; ii++){
    h_Channel_Vs_level1Id[ii].resize(8);
    h_Pdo_Vs_Channel[ii].resize(8);
    SubDirName1_Boards[ii].resize(8);
    SubDirName2_Boards[ii].resize(8);
  }

  for(int ii=0; ii<8; ii++){
    
    for(int jj=0; jj<8; jj++){

      string name_Channel_Vs_Level1Id, name_h_Pdo_Vs_Channel;
      
      name_Channel_Vs_Level1Id = "Channel_Vs_Level1Id_Board_"+
	std::to_string(analMinidaq2Obj->Board_Informations->at(ii)->SCA_ID)+"_VMM_"+std::to_string(jj);
      name_h_Pdo_Vs_Channel = "Pdo_Vs_Channel_Board_"+
	std::to_string(analMinidaq2Obj->Board_Informations->at(ii)->SCA_ID)+"_VMM_"+std::to_string(jj);
      
      h_Channel_Vs_level1Id[ii][jj]=new TH2F(name_Channel_Vs_Level1Id.c_str(),name_Channel_Vs_Level1Id.c_str(),66000,-0.5,65999.5,64,-0.5,63.5);
      h_Channel_Vs_level1Id[ii][jj]->SetDirectory(0);
      h_Pdo_Vs_Channel[ii][jj]=new TH2F(name_h_Pdo_Vs_Channel.c_str(),name_h_Pdo_Vs_Channel.c_str(),64,-0.5,63.5,1024,-0.5,1023.5);
      h_Pdo_Vs_Channel[ii][jj]->SetDirectory(0);

    }
  }


}

void Anal_NTuples::CreateHistDir(){

  for(int ii=0; ii<8;ii++){
    
    if(analMinidaq2Obj->Board_Informations->at(ii)->SCA_ID==0xFFFFFFFF) continue;

    for(int jj=0; jj<8; jj++){

      if(!analMinidaq2Obj->Board_Informations->at(ii)->VMMs_Configured->at(jj)) continue;
      
      DirName_Boards[ii] = "Board_"+std::to_string(analMinidaq2Obj->Board_Informations->at(ii)->SCA_ID)+"/";
      SubDirName1_Boards[ii][jj] = DirName_Boards[ii]+"Channel_Vs_level1Id/";
      SubDirName2_Boards[ii][jj] = DirName_Boards[ii]+"Pdo_Vs_Channel/";
      
      oFile->mkdir(DirName_Boards[ii].c_str());
      oFile->mkdir(SubDirName1_Boards[ii][jj].c_str());
      oFile->mkdir(SubDirName2_Boards[ii][jj].c_str());

    }
  }

}

void Anal_NTuples::Loop()
{
  
  if (fChain == 0) {std::cout << "empty tree" << std::endl; return;}

  Long64_t nentries = fChain->GetEntriesFast();
  cout << "nentries " << nentries << endl;

  Long64_t nbytes = 0, nb = 0;
  int decade = 0;

  //========================== Defining variables ============================//                                                                                                  
  int event_number=0;

  for (Long64_t jentry=0; jentry<nentries; jentry++) {

    double progress = 10.0 * jentry / (1.0 * nentries);
    int k = int (progress);
    if (k > decade) cout << 10 * k << " %" << endl;
    decade = k;

    //===== read this entry =====
    
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    nb = fChain->GetEntry(jentry);   nbytes += nb;
    // if (Cut(ientry) < 0) continue;

    fChain->SetBranchStatus("*",0); //disable all branches                                                                                                                      
    fChain->SetBranchStatus("m_elink_id",1);
    fChain->SetBranchStatus("m_l1_id",1);
    fChain->SetBranchStatus("m_vmm_id",1);
    fChain->SetBranchStatus("m_chan",1);
    fChain->SetBranchStatus("m_pdo",1);
    fChain->SetBranchStatus("m_tdo",1);
    fChain->SetBranchStatus("m_bcid_rel",1);

    fChain->SetBranchStatus("*",0); //disable all branches                                                                                                                           
    fChain->SetBranchStatus("m_elink_id",1);
    fChain->SetBranchStatus("m_l1_id",1);
    fChain->SetBranchStatus("m_vmm_id",1);
    fChain->SetBranchStatus("m_chan",1);
    fChain->SetBranchStatus("m_pdo",1);
    fChain->SetBranchStatus("m_tdo",1);
    fChain->SetBranchStatus("m_bcid_rel",1);

    event_number+=1;
    
    int Total_No_Hits;
    Total_No_Hits=m_vmm_id->size();

    for(int ii=0; ii<Total_No_Hits; ii++){
      
      int iBoard = m_elink_id->at(ii)/4;

      h_Channel_Vs_level1Id[iBoard][m_vmm_id->at(ii)]->Fill(m_l1_id,m_chan->at(ii));
      h_Pdo_Vs_Channel[iBoard][m_vmm_id->at(ii)]->Fill(m_chan->at(ii),m_pdo->at(ii));
      
    }

  }


}


void Anal_NTuples::End(){

  for(int ii=0; ii<8; ii++){
    
    if(analMinidaq2Obj->Board_Informations->at(ii)->SCA_ID==0xFFFFFFFF) continue;

    for(int jj=0; jj<8; jj++){

      if(!analMinidaq2Obj->Board_Informations->at(ii)->VMMs_Configured->at(jj)) continue;

      oFile->cd(SubDirName1_Boards[ii][jj].c_str());
      h_Channel_Vs_level1Id[ii][jj]->Write();
      oFile->cd(SubDirName2_Boards[ii][jj].c_str());
      h_Pdo_Vs_Channel[ii][jj]->Write();
      
    }
  }

  oFile->Close();

}
