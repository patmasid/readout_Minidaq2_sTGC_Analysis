#include "../include/Anal_Minidaq2_readout.h"

//using namespace std;

Anal_Minidaq2_readout::Anal_Minidaq2_readout( QString inputXml_, QString inputHistosRoot_){

  inputXml = inputXml_;
  inputHistosRoot = inputHistosRoot_;
  
  Board_Informations = new std::vector<Board_Info*>;

  std::cout <<"declared board information" << std::endl;
  
  Init();
  verbose = false;
  
}

void Anal_Minidaq2_readout::SetVerbose(bool v) {
  verbose = v;
}

void Anal_Minidaq2_readout::Clear() {

  m_strOutput.clear();

}


void Anal_Minidaq2_readout::Init(){

  std::cout <<"Init Run " << std::endl;
  
  Board_Informations->resize(0);

  for ( uint iboard=0; iboard<8; iboard++ ) {
    Board_Info * temp = new Board_Info();
    Board_Informations->push_back(temp);
  }

  std::cout << "init board info" << std::endl;
  
  //----------------------------------------//
  
  v_boardsChecked.resize(8);
  max_binContent.resize(8);
  VMMsize.resize(8);
  for(unsigned int ii=0; ii<8; ii++){
    max_binContent[ii].resize(8);
  }

  std::cout << "loaded histos" << std::endl;
  
  _inputHistfile=TFile::Open(inputHistosRoot.toStdString().c_str());

  std::cout << "loading config handler" << std::endl;
  
  m_ConfigHandlerMiniDAQ = new ConfigHandlerMiniDAQ();

  if(m_ConfigHandlerMiniDAQ->LoadMiniDAQConfig_from_File(inputXml)) m_global = m_ConfigHandlerMiniDAQ->miniDAQ_Global_Settings();

  std::cout << "loaded config handler" << std::endl;
}

bool Anal_Minidaq2_readout::GetInfoFromXml(){
 
  for(unsigned int iBoard=0; iBoard<m_global.board_SCA_IDs.size(); iBoard++){

    Board_Informations->at(iBoard)->SCA_ID  = m_global.board_SCA_IDs.at(iBoard);
    Board_Informations->at(iBoard)->is_pFEB = m_global.board_is_pFEB.at(iBoard);

    uint32_t Current_VMM_Config_Mask        = m_global.board_VMM_configed_mask.at(iBoard);
    
    for ( uint ivmm=0; ivmm < Board_Informations->at(iBoard)->VMMs_Configured->size(); ivmm++ ) {

      if ( Current_VMM_Config_Mask & ( 0b1 << ivmm ) ) {      
	Board_Informations->at(iBoard)->Add_New_Configed_VMM( ivmm );
      }
      
    }

  }

  return 1;

}

bool Anal_Minidaq2_readout::GetInfoFromHistos(){

  std::string inputFile = inputHistosRoot.toStdString();

  std::vector<int> binRange(2);
  if(inputFile.find("ch0_ch31") != std::string::npos) { binRange[0]=1;  binRange[1]=32; ChannelRange="0_to_31"; }
  else if(inputFile.find("ch32_ch63") != std::string::npos) { binRange[0]=33;  binRange[1]=64; ChannelRange="32_to_63"; }

  _inputHistfile->cd();

  //----------------------------------------------------------//
  //                  Open Run Root File
  //----------------------------------------------------------//
  
  TIter next(_inputHistfile->GetListOfKeys());
  TKey *key;
  while ((key = (TKey*)next())) {
    TClass *cl = gROOT->GetClass(key->GetClassName());
    if (!cl->InheritsFrom("TH1")) continue;
    TH1 *h = (TH1*)key->ReadObj();
    //std::cout<<h->GetName()<<std::endl;
    
    std::string hist_name = h->GetName();

    //-----------------------------------------------------------------------//                                                                                                                        
    // Getting values of board, VMM and channel from the histogram                                                                                                                                     
    //-----------------------------------------------------------------------//                                                                                                                  

    std::ifstream myhist(hist_name.c_str());

    std::string line;

    std::stringstream ss(hist_name);
    std::string item;
    std::vector<std::string> splittedStrings;
    while (std::getline(ss, item, '_'))
      {
        splittedStrings.push_back(item);
      }

    //----------------------- printing all the parts of a histogram ---------------------------//                                                                                              
    //for(int iS=0; iS<splittedStrings.size(); iS++){                                                                                                                                    

    //if(iS==(splittedStrings.size()-1)) std::cout<<splittedStrings[iS]<<std::endl;                                                                                                      
    //else std::cout<<splittedStrings[iS]<<" ";                                                                                                                                            

    //}                                                                                                                                                                          

    //---------------------- Extracting values of iBoard, iVMM, iChan ---------------------------//                                                                                 
    
    int elink,iBoard, iVMM, iChan;

    std::istringstream issElink(splittedStrings[1]);
    std::istringstream issVMM(splittedStrings[3]);
    issElink >> elink;
    issVMM >> iVMM;
    //if (issElink.fail()) {
    //std::cout<<"Elink not stored as an integer "<<elink<<std::endl;
    //}
    //if (issVMM.fail()) {
    //std::cout<<"VMM not stored as an integer "<<iVMM<<std::endl;
    //}


    iBoard = elink/4;
    
    Board_Info * iboard_info = Board_Informations->at(iBoard);

    if(splittedStrings[splittedStrings.size()-1]=="hits") {

      max_binContent[iBoard][iVMM] = h->GetBinContent(h->GetMaximumBin());
      v_boardsChecked[iBoard]=1;
      iboard_info->Add_New_Found_VMM_Histo(iVMM);


      //--------------------------------------------------------------------------//                                                                                                   
      //    Only Look for Hits within the Channel Range that has been configured                                                                                               
      //--------------------------------------------------------------------------//                                                                                               

      for(int iBin=binRange[0]; iBin<=binRange[1]; iBin++){

        //------------------------------------------------------------------------//                                                                                               
        //                  If No Hits then Add Dead Channel                                                                                                             
        //------------------------------------------------------------------------//                                                                                   

        //if(iBin==12) std::cout<<"Board "<<iBoard<<" VMM"<<iVMM<<" BinContent in channel 11 "<<h->GetBinContent(iBin)<<std::endl;                                                      
        if(h->GetBinContent(iBin)==0) iboard_info->Add_New_Dead_Channel(iVMM, iBin-1);

        //------------------------------------------------------------------------//                                                                                  
        //           If # of Hits Less than Max, Add Inefficient Channel                                                                                          
        //------------------------------------------------------------------------// 
	
	else if(h->GetBinContent(iBin)< max_binContent[iBoard][iVMM]){
          iboard_info->Add_New_Ineff_Channel( iVMM, iBin-1, (h->GetBinContent(iBin)/max_binContent[iBoard][iVMM]) );
        }

      }

    }

    else if(splittedStrings[splittedStrings.size()-1]=="pdo") {

      std::istringstream issChannel(splittedStrings[5]);
      issChannel >> iChan;
      if (issChannel.fail()) {
	std::cout<<"Channel not stored as an integer "<<std::endl;
      }

      std::pair<int,double> currentLowPdoPair;

      double binMean;

      binMean=h->GetMean(1);
      //std::cout<<"channel "<<iChan<<"Bin mean "<<binMean<<std::endl;                                                                                                                 
      //------------------------------------------------------------------------//                                                                                       
      //         Treat VMM 0 and VMM 1 + 2 differently for pdo test for pFEB
      //         All VMM has gain 1 for sFEB                                    //                                                                                                           
      //------------------------------------------------------------------------//                                                                                      

      bool isLowPDO  = false;
      bool isHighPDO = true;

      if ( iboard_info->is_pFEB ) { //pFEFB                                                                                                                                           
        if( iVMM==0 ) {
          if (h->GetMean(1) > 200 ) {
            isHighPDO = true;
          }
          else if ( h->GetMean(1) < 100 ) {
            isLowPDO = true;
          }
        }
        else if( iVMM==1 || iVMM==2) {
          if ( h->GetMean(1) > 750 ) {
            isHighPDO = true;
          }
          else if ( h->GetMean(1) < 375 ) {
            isLowPDO = true;
          }
        }
      }

      else { //sFEB                                                                                                                                                                
        if( h->GetMean(1) > 300 ) {
          isHighPDO = true;
        }
        else if ( h->GetMean(1) < 150 ) {
          isLowPDO = true;
        }
      }

      if ( isHighPDO ) iboard_info->Add_New_HighPDOChannel(iVMM, (iChan), h->GetMean(1));
      if ( isLowPDO )  iboard_info->Add_New_LowPDOChannel( iVMM, (iChan), h->GetMean(1));

    }

    //---------------------------------------------------------//                                                                                                                  
    //               Output Low PDO channels                                                                                                                                      
    //---------------------------------------------------------//                                                                                                                       

  }

  return 1;
}

  
//--------------------------------------------------//
//               Output Run Summary
//--------------------------------------------------//

bool Anal_Minidaq2_readout::PrintReport_for8Boards_Sheet(){

  std::string inputFileName = inputHistosRoot.toStdString();

  std::string outputFileName;

  outputFileName = inputFileName;
  outputFileName.erase(outputFileName.length()-5);

  outputFileName += "_TABLE.txt";

  m_strOutput.open(outputFileName.c_str());
  
  m_strOutput << "Channels_Checked "<<ChannelRange<<std::endl;

  //--------------------------------------------------//
  //            Output Boards Connected
  //--------------------------------------------------//
  
  m_strOutput << "Boards_Connected ";
  
  for(unsigned int iBoard=0; iBoard<Board_Informations->size(); iBoard++){

    if(Board_Informations->at(iBoard)->SCA_ID==0xFFFFFFFF) continue;

    m_strOutput << iBoard << ",";

  }

  m_strOutput << std::endl;

  //--------------------------------------------------//
  //            Output Boards With Data
  //--------------------------------------------------//
  
  m_strOutput << "Boards_Present ";

  for(unsigned int iB=0; iB<v_boardsChecked.size(); iB++){

    if(!v_boardsChecked[iB]) continue;

    m_strOutput << iB <<",";

  }

  m_strOutput << std::endl;

  //--------------------------------------------------//
  //            Output Boards Missing in Run
  //--------------------------------------------------//
  
  m_strOutput << "Boards_Missing ";

  for(unsigned int iB=0; iB<v_boardsChecked.size(); iB++){

    if(v_boardsChecked[iB] || Board_Informations->at(iB)->SCA_ID==4294967295) continue;

    m_strOutput << iB <<",";

  }

  m_strOutput << std::endl;

  //--------------------------------------------------//
  //            Output Per Board Information
  //--------------------------------------------------//
  
  m_strOutput << "BoardNo "<<"SCA-ID "<< "Type "<<"VMMs_Configured "<<"VMMs_NoHistos "<<"VMM_0_DeadChan "<< "VMM_0_MaxHits "<< "VMM_0_lessHitsChan "<< "VMM_0_lowhighpdoChan " <<"VMM_1_DeadChan "<< "VMM_1_MaxHits "<<"VMM_1_lessHitsChan " << "VMM_1_lowhighpdoChan " <<"VMM_2_DeadChan "<< "VMM_2_MaxHits "<<"VMM_2_lessHitsChan " << "VMM_2_lowhighpdoChan ";
  
  m_strOutput <<"VMM_3_DeadChan "<< "VMM_3_MaxHits "<<"VMM_3_lessHitsChan " << "VMM_3_lowhighpdoChan "<<"VMM_4_DeadChan " << "VMM_4_MaxHits "<<"VMM_4_lessHitsChan " << "VMM_4_lowhighpdoChan "<<"VMM_5_DeadChan " << "VMM_5_MaxHits "<<"VMM_5_lessHitsChan " << "VMM_5_lowhighpdoChan " <<"VMM_6_DeadChan " "VMM_6_MaxHits "<<"VMM_6_lessHitsChan " << "VMM_6_lowhighpdoChan "<<"VMM_7_DeadChan " << "VMM_7_MaxHits "<<"VMM_7_lessHitsChan " << "VMM_7_lowhighpdoChan ";
  
  m_strOutput << std::endl;

  //--------------------------------------------------//
  //              Loop Over Boards
  //--------------------------------------------------//
  
  for(unsigned int iBoard=0; iBoard<Board_Informations->size(); iBoard++){

    Board_Info * iboard_info = Board_Informations->at(iBoard);

    //-----------------------------------------------------//
    //    Continue if board is not configured/has SCA ID
    //-----------------------------------------------------//

    if(iboard_info->SCA_ID==0xFFFFFFFF) continue;
    
    m_strOutput << iBoard <<" "; 

    m_strOutput << iboard_info->SCA_ID <<" ";

    //-----------------------------------------------------//
    //              Output is pFEB or sFEB
    //-----------------------------------------------------//
    
    if (iboard_info->is_pFEB) m_strOutput << "pFEB ";
    else                      m_strOutput << "sFEB ";

    //-----------------------------------------------------//
    //              Output nVMMs on Board
    //-----------------------------------------------------//
    
    int iVMMMax;
    if(iboard_info->is_pFEB) iVMMMax=3;
    else                     iVMMMax=8;
    
    //------------------------------------------------------------//

    if(v_boardsChecked[iBoard]==0) { m_strOutput << std::endl; continue; }

    //------------------------------------------------------------//
    //                 Output Configured VMMs
    //------------------------------------------------------------//
    
    for(unsigned int iVMM=0; iVMM<iboard_info->VMMs_Configured->size(); iVMM++){

      if( !(iboard_info->VMMs_Configured->at(iVMM) ) )  continue;
      
      m_strOutput << iVMM << ",";
      
    }

    m_strOutput << " ";

    //------------------------------------------------------------//
    //           Check to See if All Boards Configed
    //------------------------------------------------------------//
    
    bool AllVMMConfiged = true;
      
    for(unsigned int iVMM=0; iVMM<iVMMMax; iVMM++){

      if( !(iboard_info->VMMs_Configured->at(iVMM) ) ) {
	AllVMMConfiged = false;
      }
      
      if( !iboard_info->hist_Found->at(iVMM) ) {
	m_strOutput << iVMM << ",";
	AllVMMConfiged = false;
      }
    }

    if(AllVMMConfiged) m_strOutput << "------";

    m_strOutput << " ";

    //-----------------------------------------------------------//
    
    for(unsigned int iVMM=0; iVMM<iboard_info->VMMs_Configured->size(); iVMM++){
      
      //--------------------------------------------------------//

      if( !(iboard_info->VMMs_Configured->at(iVMM)) && iVMM<iVMMMax ) { m_strOutput << "------ ------ ------ ------ "; continue;}
      else if( !(iboard_info->VMMs_Configured->at(iVMM)) ) continue;
      
     
      if(iboard_info->v_dead_channels->at(iVMM)->empty()) { m_strOutput << "------ ";}
      
      else{
 
	for(unsigned int ii=0; ii<iboard_info->v_dead_channels->at(iVMM)->size(); ii++){
	  
	  m_strOutput << iboard_info->v_dead_channels->at(iVMM)->at(ii) <<",";
	  
	}
	m_strOutput << " ";

      }
      
      //----------------------------------------------------------//

      m_strOutput << max_binContent[iBoard][iVMM] <<" ";

      if(iboard_info->v_lessHitsChannels->at(iVMM)->empty()) { m_strOutput << "------ ";}
      
      else {
	for(unsigned int jj=0; jj<iboard_info->v_lessHitsChannels->at(iVMM)->size(); jj++){
	  
	  m_strOutput << iboard_info->v_lessHitsChannels->at(iVMM)->at(jj).first
		      <<"("<<iboard_info->v_lessHitsChannels->at(iVMM)->at(jj).second<< ")"<<",";
	  
	}

	m_strOutput << " ";

      }

      if(iboard_info->v_lowpdoChannels->at(iVMM)->empty()) { m_strOutput << "------ "; }
      
      else {
	std::cout<<iboard_info->v_lowpdoChannels->at(iVMM)->size()<<std::endl;
        for(unsigned int ich=0; ich<iboard_info->v_lowpdoChannels->at(iVMM)->size(); ich++){

          m_strOutput << iboard_info->v_lowpdoChannels->at(iVMM)->at(ich).first
		      <<"("<<iboard_info->v_lowpdoChannels->at(iVMM)->at(ich).second<< ")"<<",";

	}

	m_strOutput << " ";

      }

    }
    
    m_strOutput << std::endl;


  }
  
  return 1;


}

bool Anal_Minidaq2_readout::PrintReport_for8Boards(){
  
  std::string inputFileName = inputHistosRoot.toStdString();
  
  std::string outputFileName;
  
  outputFileName = inputFileName;
  outputFileName.erase(outputFileName.length()-5);
  
  outputFileName += "_REPORT.txt"; 

  std::cout<<"Output file name: "<<outputFileName<<std::endl;

  m_strOutput.open(outputFileName.c_str());

  m_strOutput << "#####################################################################################################" << std::endl;
  m_strOutput << "#################################### Readout Report MiniDAQ2 ########################################" << std::endl;
  
  m_strOutput << "#####################################################################################################" << std::endl;

  m_strOutput << std::endl;

  m_strOutput << "Channels Checked: "<<ChannelRange<<std::endl;
  
  m_strOutput << std::endl;
  
  m_strOutput << std::endl;

  m_strOutput << "Boards Connected: ";
  
  for(unsigned int iBoard=0; iBoard<Board_Informations->size(); iBoard++){
    
    if(Board_Informations->at(iBoard)->SCA_ID==0xFFFFFFFF) continue;
    
    m_strOutput << iBoard << " ";
    
  }

  m_strOutput << std::endl;
  
  m_strOutput << "Boards Present in the output: ";

  for(unsigned int iB=0; iB<v_boardsChecked.size(); iB++){
    
    if(!v_boardsChecked[iB]) continue;
    
    m_strOutput << iB <<" ";

  }

  m_strOutput << std::endl;

  m_strOutput << "Boards Missing in the output: ";

  for(unsigned int iB=0; iB<v_boardsChecked.size(); iB++){

    if ( iB >= Board_Informations->size() ) {
      std::cout << "Error: WTF Board_Informations out of bounds";
      break;
    }
    
    if(v_boardsChecked[iB] || Board_Informations->at(iB)->SCA_ID==0xFFFFFFFF) continue;

    m_strOutput << iB <<" ";

  }

  m_strOutput << std::endl;
  
  m_strOutput << std::endl;

  m_strOutput << "#####################################################################################################" << std::endl;

  m_strOutput << "#####################################################################################################" << std::endl;

  m_strOutput << std::endl;

  for(unsigned int iBoard=0; iBoard<Board_Informations->size(); iBoard++){

    Board_Info * iboard_info = Board_Informations->at(iBoard);
    
    if(iboard_info->SCA_ID==0xFFFFFFFF)continue;

    m_strOutput << "Board "<<iBoard<<": "<<std::endl;
    
    m_strOutput << std::endl;
    
    m_strOutput << "Board SCA ID: " << iboard_info->SCA_ID <<std::endl;
    
    m_strOutput << std::endl;
    
    m_strOutput << "Board Type: " ;
    if (iboard_info->is_pFEB) m_strOutput << "pFEB" <<std::endl;
    else                      m_strOutput << "sFEB" <<std::endl; 
    
    m_strOutput << std::endl;

    if(v_boardsChecked[iBoard]==0) { 
      m_strOutput << "#####################################################################################################" << std::endl; 
      m_strOutput << "#####################################################################################################" << std::endl;
      m_strOutput << std::endl;
      continue;
    }

    m_strOutput << "VMMs Configured: ";
    
    for(unsigned int iVMM=0; iVMM<iboard_info->VMMs_Configured->size(); iVMM++){

      if( !( iboard_info->VMMs_Configured->at(iVMM)) ) continue;
      
      m_strOutput << iVMM << " ";

    }

    m_strOutput << std::endl;
    
    m_strOutput << "VMMs - No Histogram found: ";

    for(unsigned int iVMM=0; iVMM<iboard_info->hist_Found->size(); iVMM++){

      if( !(iboard_info->VMMs_Configured->at(iVMM)) ) continue;
      if(   iboard_info->hist_Found->at(iVMM) ) m_strOutput << iVMM << " ";

    }

    m_strOutput << std::endl;
    
    m_strOutput << std::endl;
    
    m_strOutput << "#####################################################################################################" << std::endl;

    for(unsigned int iVMM=0; iVMM<iboard_info->VMMs_Configured->size(); iVMM++){
      
      if( !(iboard_info->VMMs_Configured->at(iVMM)) ) continue;
      
      m_strOutput << "VMM "<<iVMM<<": "<<std::endl;
      
      m_strOutput << "Channels with 0 hits: ";
      
      for(unsigned int ii=0; ii<iboard_info->v_dead_channels->at(iVMM)->size(); ii++){
	
	m_strOutput << iboard_info->v_dead_channels->at(iVMM)->at(ii) <<" ";

      }

      m_strOutput << std::endl;

      m_strOutput << "Maximum no. of hits: " << max_binContent[iBoard][iVMM] <<std::endl;

      m_strOutput << "Channels with lesser hits (compared to the maximum): ";

      for(unsigned int jj=0; jj<iboard_info->v_lessHitsChannels->at(iVMM)->size(); jj++){
	
	m_strOutput << iboard_info->v_lessHitsChannels->at(iVMM)->at(jj).first
		    <<"("<<iboard_info->v_lessHitsChannels->at(iVMM)->at(jj).second<< ")";

      }

      m_strOutput << std::endl;
      
      m_strOutput << "Channels with higher or lower pdo: ";

      for(unsigned int ich=0; ich<iboard_info->v_lowpdoChannels->at(iVMM)->size(); ich++){

        m_strOutput << iboard_info->v_lowpdoChannels->at(iVMM)->at(ich).first
		    <<"("<<iboard_info->v_lowpdoChannels->at(iVMM)->at(ich).second<< ")";

      }

      m_strOutput << std::endl;

      m_strOutput << "#####################################################################################################" << std::endl;
      
    }

    m_strOutput << "#####################################################################################################" << std::endl;
    
    m_strOutput << std::endl;

  }

  m_strOutput.close();

  return 1;

}


Anal_Minidaq2_readout::~Anal_Minidaq2_readout(){
  
}
