#include "../include/Ana_Minidaq2_Summary.h"

//using namespace std;

Ana_Minidaq2_Summary::Ana_Minidaq2_Summary(std::vector<QString> *run_names, std::vector<QString> *run_dir){
  Init(run_names, run_dir);
  verbose = false;
  Board_Informations_Combined = new std::vector<Board_Info*>;
}

void Ana_Minidaq2_Summary::SetVerbose(bool v) {
  verbose = v;
}

void Ana_Minidaq2_Summary::Clear() {

  m_strOutput.clear();

}

void Ana_Minidaq2_Summary::Init(std::vector<QString> *run_names, std::vector<QString> *run_dir){

  run_analyzers = new std::vector<Anal_Minidaq2_readout*>;
  run_analyzers->resize(0);
  run_ntuples   = new std::vector<Anal_NTuples*>;
  run_ntuples->resize(0);

  for ( uint irun = 0; irun<run_names->size(); irun++ ) {

    QString inputXmlFile    = run_dir->at(irun) + "/" + run_names->at(irun) + "/" +
      QString::fromStdString("/ASIC_config_at_start_of_run/miniDAQ_current_tmp.xml");
    QString inputHistosRoot = run_dir->at(irun) + "/" + run_names->at(irun) + "/" +
      run_names->at(irun) + QString::fromStdString("_decoded.root");

    std::string outFile_Name;
    outFile_Name = inputHistosRoot.toStdString();
    outFile_Name.erase(outFile_Name.length()-12);
    outFile_Name += "Hists.root";

    std::cout << "adding run " << inputXmlFile.toStdString() << " "
	      << inputHistosRoot.toStdString() << " to analysis " << std::endl;
    
    Anal_Minidaq2_readout * anaReadout = new Anal_Minidaq2_readout(inputXmlFile,inputHistosRoot);
    run_analyzers->push_back(anaReadout);
    
    Anal_NTuples * anaNTuples = new Anal_NTuples(inputXmlFile,inputHistosRoot,outFile_Name);
    run_ntuples->push_back(anaNTuples);

    std::cout << "added run " << inputHistosRoot.toStdString() << " to analysis " << std::endl;
    
  }

}

void Ana_Minidaq2_Summary::RunAnalysis() {

  std::cout << "running analysis" << std::endl;
  
  //-----------------------------------------------------------//
  //              Run Analysis on each Run
  //-----------------------------------------------------------//
  
  for ( uint irun = 0; irun<run_analyzers->size(); irun++ ) {

    run_analyzers->at(irun)->GetInfoFromXml();
    run_analyzers->at(irun)->GetInfoFromHistos();
    run_analyzers->at(irun)->PrintReport_for8Boards_Sheet();

  }

  //----------------------------------------------------------//
  //              Plot 2D histograms for each run
  //----------------------------------------------------------//
  
  for ( uint irun = 0; irun<run_ntuples->size(); irun++ ) {

    run_ntuples->at(irun)->InitMapHistograms(run_analyzers->at(irun));
    run_ntuples->at(irun)->CreateHistDir();
    run_ntuples->at(irun)->Loop();
    run_ntuples->at(irun)->End();

  }
  

}

//---------------------------------------------------------------//
//     Combine Information for the same board from all Runs 
//---------------------------------------------------------------//

void Ana_Minidaq2_Summary::CombineAnalyzedData() {

  std::cout << "combining analysis" << std::endl;

  //---------------------------------------------------------------------------//
  //                     Loop over all runs processed
  //---------------------------------------------------------------------------//
  
  for ( uint irun = 0; irun<run_analyzers->size(); irun++ ) {

    std::cout << "adding new run " << std::endl;
    
    //        Get analyzer for the ith run
    Anal_Minidaq2_readout *    irun_analyzer = run_analyzers->at(irun);
    //        Get Board Information of the analyzed run
    std::vector<Board_Info*> * irun_board_informations = irun_analyzer->Board_Informations;

    //------------------------------------------------------------------------------//
    //                   Loop over all boards in the run
    //------------------------------------------------------------------------------//

    for ( uint iboard = 0; iboard < irun_board_informations->size(); iboard++ ) {

      //     Get Board Information for iboard and board's SCA ID
      std::cout << " getting board info for board " << iboard << std::endl;
      
      Board_Info * iboard_info = irun_board_informations->at(iboard);
      int iboard_SCA_ID = iboard_info->SCA_ID;

      std::cout << iboard_info->SCA_ID << std::endl;
      
      if ( iboard_info->SCA_ID == 0xFFFFFFFF ) {
	continue;
      }
      
      //--------------------------------------------------------------//
      //            Check to see if Boards Already Registered
      //--------------------------------------------------------------//
      
      bool New_Board = true;

      //-----------------------------------------------------------------------------//
      //         Note: Board_Informations_Combined is not a pointer. 
      //               Don't want Board_Information_Combined to point to
      //               a board_info within a run_analysis.  Instead
      //               Board_Information_Combined needs its own Board_Info
      //               objects
      //-----------------------------------------------------------------------------//
      for ( uint jboard_detected=0; jboard_detected < Board_Informations_Combined->size(); jboard_detected++ ) {

	//---------------------------------------------------------------------------------------//
	//      If the board SCA ID already exists from a previous run then combine the info
	//---------------------------------------------------------------------------------------//
	
	if ( Board_Informations_Combined->at(jboard_detected)->SCA_ID == iboard_SCA_ID ) {
	  Board_Informations_Combined->at(jboard_detected)->UploadInfo(iboard_info);
	  New_Board = false;
	  break;
	}
       
      } // end of loop over Board_Informations_Combined

      //---------------------------------------------------------------------------------------//
      //      If the board SCA ID does not exist then push the information to the back
      //---------------------------------------------------------------------------------------//

      if ( New_Board ) {
	std::cout << "adding new board" << iboard_SCA_ID << std::endl;
	Board_Info * new_board = new Board_Info();
	new_board->SCA_ID  = iboard_SCA_ID;
	new_board->is_pFEB = iboard_info->is_pFEB;
	Board_Informations_Combined->push_back( new_board );
	Board_Informations_Combined->back()->UploadInfo( iboard_info );
      }

      std::cout << "end of iboard" << std::endl;
      
    }// end of iboard    

  } // end of irun

}

void Ana_Minidaq2_Summary::PrintSummaryFile(QString outfile_txt) {

  std::cout << "printing summary file" << std::endl;
  
  m_strOutput.open(outfile_txt.toStdString().c_str());
  
  m_strOutput << "SCA-ID " << "Serial_Number " << "Type " << "VMMs_Configured " << "Whole_VMM_no_Output_Digital "
	      << "VMM0_Ch_No_Digi "  << "VMM1_Ch_No_Digi "  << "VMM2_Ch_No_Digi "  << "VMM3_Ch_No_Digi "
	      << "VMM4_Ch_No_Digi "  << "VMM5_Ch_No_Digi "  << "VMM6_Ch_No_Digi "  << "VMM7_Ch_No_Digi "
	      << "VMM0_Ch_Ineff "    << "VMM1_Ch_Ineff "    << "VMM2_Ch_Ineff "    << "VMM3_Ch_Ineff "
	      << "VMM4_Ch_Ineff "    << "VMM5_Ch_Ineff "    << "VMM6_Ch_Ineff "    << "VMM7_Ch_Ineff "
	      << "VMM0_Ch_Low_PDO "  << "VMM1_Ch_Low_PDO "  << "VMM2_Ch_Low_PDO "  << "VMM3_Ch_Low_PDO "
	      << "VMM4_Ch_Low_PDO "  << "VMM5_Ch_Low_PDO "  << "VMM6_Ch_Low_PDO "  << "VMM7_Ch_Low_PDO "
	      << "VMM0_Ch_High_PDO " << "VMM1_Ch_High_PDO " << "VMM2_Ch_High_PDO " << "VMM3_Ch_High_PDO "
	      << "VMM4_Ch_High_PDO " << "VMM5_Ch_High_PDO " << "VMM6_Ch_High_PDO " << "VMM7_Ch_High_PDO " << std::endl;
  
  for ( uint iboard=0; iboard < Board_Informations_Combined->size(); iboard++ ) {

    Board_Info * iboard_info = Board_Informations_Combined->at(iboard);
    
    m_strOutput << iboard_info->SCA_ID << " "; // SCA-ID
    m_strOutput << "------ "; // Serial Number

    // Type (pFEB or sFEB)
    if ( iboard_info->is_pFEB) m_strOutput << "pFEB ";
    else                      m_strOutput << "sFEB ";

    //------------------------------------------------------------//
    //                 Output Configured VMMs
    //------------------------------------------------------------//

    for(unsigned int iVMM=0; iVMM<iboard_info->VMMs_Configured->size(); iVMM++){

      if( !(iboard_info->VMMs_Configured->at(iVMM) ) )  continue;

      m_strOutput << iVMM << ",";

    }

    m_strOutput << " ";

    //------------------------------------------------------------//
    //           Check to See if All Boards Configed
    //------------------------------------------------------------//

    bool AllVMMConfiged = true;

    for(unsigned int iVMM=0; iVMM<iboard_info->VMMs_Configured->size(); iVMM++){

      if( !(iboard_info->VMMs_Configured->at(iVMM) ) ) {
	AllVMMConfiged = false;
      }

      if( !iboard_info->hist_Found->at(iVMM) ) {
	m_strOutput << iVMM << ",";
	AllVMMConfiged = false;
      }
    }

    if(AllVMMConfiged) m_strOutput << "------";

    m_strOutput << " ";


    //-------------------------------------------------------------//
    //                Output No Digital Hits channels
    //-------------------------------------------------------------//

    for(unsigned int iVMM=0; iVMM<iboard_info->v_dead_channels->size(); iVMM++){

      //--------------------------------------------------------//
      
      if(iboard_info->v_dead_channels->at(iVMM)->empty()) { m_strOutput << "------";}
      else{
	
        for(unsigned int ii=0; ii<iboard_info->v_dead_channels->at(iVMM)->size(); ii++){
	  m_strOutput << iboard_info->v_dead_channels->at(iVMM)->at(ii) << ",";
	}
      }
      
      m_strOutput << " ";
    
    }

    //-------------------------------------------------------------//
    //               Output Inefficient Channels
    //-------------------------------------------------------------//
    
    for(unsigned int iVMM=0; iVMM<iboard_info->v_lessHitsChannels->size(); iVMM++){

      if(iboard_info->v_lessHitsChannels->at(iVMM)->empty()) { m_strOutput << "------";}

      else {
	for(unsigned int jj=0; jj<iboard_info->v_lessHitsChannels->at(iVMM)->size(); jj++){
	  m_strOutput << iboard_info->v_lessHitsChannels->at(iVMM)->at(jj).first
		      <<"("<<iboard_info->v_lessHitsChannels->at(iVMM)->at(jj).second<< ")"<<",";
	}

      }
      m_strOutput << " ";
    }

    //------------------------------------------------------------//
    //                 Output Low PDO Channels
    //------------------------------------------------------------//

    for(unsigned int iVMM=0; iVMM<iboard_info->v_lowpdoChannels->size(); iVMM++){

      if(iboard_info->v_lowpdoChannels->at(iVMM)->empty()) { m_strOutput << "------"; }

      else {
	for(unsigned int ich=0; ich<iboard_info->v_lowpdoChannels->at(iVMM)->size(); ich++){
	  
	  m_strOutput << iboard_info->v_lowpdoChannels->at(iVMM)->at(ich).first
		      << "("<<iboard_info->v_lowpdoChannels->at(iVMM)->at(ich).second<<")"<<",";
	}
	
      }
      m_strOutput << " ";
    }
    
    //------------------------------------------------------------//
    //                 Output High PDO Channels
    //------------------------------------------------------------//

    for(unsigned int iVMM=0; iVMM<iboard_info->v_highpdoChannels->size(); iVMM++){

      if(iboard_info->v_highpdoChannels->at(iVMM)->empty()) { m_strOutput << "------"; }

      else {
	for(unsigned int ich=0; ich<iboard_info->v_highpdoChannels->at(iVMM)->size(); ich++){

	  m_strOutput << iboard_info->v_highpdoChannels->at(iVMM)->at(ich).first
		      << "("<<iboard_info->v_highpdoChannels->at(iVMM)->at(ich).second<<")"<<",";
	}

      }
      m_strOutput << " ";
    }

    m_strOutput << std::endl;
    
  } // end of loop over boards

  m_strOutput.flush();
  m_strOutput.close();
  
}

Ana_Minidaq2_Summary::~Ana_Minidaq2_Summary(){  

  delete run_analyzers;
  delete run_ntuples;

}
