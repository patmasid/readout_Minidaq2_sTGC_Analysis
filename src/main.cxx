#define run_cxx

#include <iostream>
#include <vector>
#include <cstring>

#include <QApplication>
#include <QObject>
#include <QList>
#include <QStringList>
#include <QHostAddress>

#include <cstring>
#include <cmath>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <algorithm>

//#include <TH2.h>                                                                                                                                                                                          
#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <array>

#include <iomanip>
#include "THStack.h"
#include <math.h>

#include "TApplication.h"
#include <TSystem.h>

#include "Anal_Minidaq2_readout.h"
#include "Ana_Minidaq2_Summary.h"
#include "Anal_NTuples.h"
#include "Board_Info.h"


using namespace std;

int main(int argc, char* argv[])
{

  if (argc < 3) {
    cerr << "Please give 3 arguments " << "input run list " << " and input run directory "<< endl;
    return -1;
  }
  const char *inputRunList            = argv[1];
  const char *inputRunDirectory       = argv[2];
  const char *outputName              = argv[3];
  
  QString inputRunList_Str        = inputRunList;
  QString inputRunDirectory_Str   = inputRunDirectory;
  QString outfile_txt             = outputName;

  std::ifstream run_list_file(inputRunList_Str.toStdString().c_str());

  std::vector<QString> * run_list = new std::vector<QString>;
  run_list->resize(0);
  std::vector<QString> * run_dir = new std::vector<QString>;
  run_dir->resize(0);

  std::string line;

  if ( run_list_file.is_open() ) {

    std::cout << "Open run List File " << inputRunList << std::endl;

    while ( std::getline ( run_list_file,line ) ) {

      std::string i_run_name =  line;
      
      run_list->push_back( QString::fromStdString(i_run_name) );
      run_dir->push_back(  inputRunDirectory_Str );
    }

  }

  Ana_Minidaq2_Summary * ana = new Ana_Minidaq2_Summary(run_list, run_dir);
  ana->RunAnalysis();
  ana->CombineAnalyzedData();
  ana->PrintSummaryFile(outfile_txt);

}
