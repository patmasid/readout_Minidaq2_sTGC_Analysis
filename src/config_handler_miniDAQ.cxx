// vmm
#include "config_handler_miniDAQ.h"
//#include "string_utils.h"

// std/stl
#include <bitset> // debugging
#include <exception>
#include <sstream>
using namespace std;

// boost
#include <boost/format.hpp>



//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  ConfigHandlerMiniDAQ -- Constructor
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
ConfigHandlerMiniDAQ::ConfigHandlerMiniDAQ(QObject *parent) :
    QObject(parent),
    m_dbg(false),
    m_msg(0)
{
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerMiniDAQ::LoadMessageHandler(MessageHandler& m)
{
    m_msg = &m;
}
//// ------------------------------------------------------------------------ //
bool ConfigHandlerMiniDAQ::LoadMiniDAQConfig_from_File(const QString &filename)
{

  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree pt;
  try {
    read_xml(filename.toStdString(), pt, trim_whitespace | no_comments);
  }
  catch(std::exception& e) {
    m_sx.str("");
    m_sx << "ERROR Unable to read configuration miniDAQ XML file: " << e.what();
    msg()(m_sx,"ConfigHandlerMiniDAQ::WriteConfig");
    return false;
  }

  m_global_miniDAQ = LoadMiniDAQGlobalSettings(pt); 
  bool global_ok = m_global_miniDAQ.ok;
  if(!global_ok) {
    cout<<"Problem loading GlobalSettings, ConfigHandlerMiniDAQ::LoadConfig"<<endl;
    msg()("Problem loading GlobalSettings", "ConfigHandlerMiniDAQ::LoadConfig");
  }

  m_ttc_miniDAQ = LoadMiniDAQTTCSettings(pt);
  bool ttc_ok = m_ttc_miniDAQ.ok;
  if(!ttc_ok) {
    cout<<"Problem loading TTCSettings, ConfigHandlerMiniDAQ::LoadConfig"<<endl;
    msg()("Problem loading TTCSettings", "ConfigHandlerMiniDAQ::LoadConfig");
  }

  return (global_ok && ttc_ok); 
}
//// ------------------------------------------------------------------------ //
void ConfigHandlerMiniDAQ::WriteMiniDAQConfig_to_File(QString filename)
{
  using boost::format;
  using boost::property_tree::ptree;
  using namespace boost::property_tree::xml_parser;
  ptree outpt;
  stringstream ss;

  ptree out_root;

  ptree out_miniDAQ_global;
  ptree out_miniDAQ_ttc;

  stringstream elink_i_str;

  //------------------------------------------------------//

  ptree out_elink;

  for ( uint i=0; i< miniDAQ_Global_Settings().elink_locked_mask.size(); i++ ) {

    elink_i_str.str("");
    elink_i_str << "elink_" << format("%01i") % i;

    out_elink.put(elink_i_str.str(), miniDAQ_Global_Settings().elink_locked_mask.at(i));
    
  }

  out_miniDAQ_global.add_child("elink_locked_mask", out_elink);

  //--------------------------------------------------------//

  out_miniDAQ_global.put("board_exist_mask",   miniDAQ_Global_Settings().board_exist_mask);

  //--------------------------------------------------------//

  ptree out_link; 

  // < phase, board, elink >
  for ( uint i=0; i<miniDAQ_Global_Settings().elink_matrix.size(); i++ ) {

    elink_i_str.str("");
    elink_i_str << "Phase_" << format("%02i") % i << "_elink_mask";

    //----------------------------------------------------------------------//
    //           Every phase has 32 links, build a mask out of them
    //----------------------------------------------------------------------//
    uint phase_mask = 0;
    
    // j = board, k = elink on board
    for ( uint j=0; j<miniDAQ_Global_Settings().elink_matrix.at(i).size(); j++ ) {
      for ( uint k=0; k<miniDAQ_Global_Settings().elink_matrix.at(i).at(j).size(); k++ ) {
	uint tmp_phase = miniDAQ_Global_Settings().elink_matrix.at(i).at(j).at(k);
	tmp_phase = tmp_phase << (j*4+k); // bitwise operation: push back by j*4+k bits
	phase_mask += tmp_phase;
      }
    }

    out_link.put(elink_i_str.str(), phase_mask);

  }
  out_miniDAQ_global.add_child("ElinkMatrix", out_link); 

  //--------------------------------------------------------//

  out_miniDAQ_global.put("configured_board_mask",   miniDAQ_Global_Settings().configured_board_mask);

  //-------------------------------------------------------------//

  // this was done correctly
  for ( uint i=0; i< miniDAQ_Global_Settings().board_SCA_IDs.size(); i++ ) {

    ptree out_board;

    ss.str("");
    ss << "board_" << format("%01i") % i;

    elink_i_str.str("");
    elink_i_str << "board_SCA_ID";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().board_SCA_IDs.at(i));
    
    elink_i_str.str("");
    elink_i_str << "board_is_pFEB";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().board_is_pFEB.at(i));
    
    elink_i_str.str("");
    elink_i_str << "board_GPIO_dir_mask";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().board_GPIO_dir_mask.at(i));
		   
    elink_i_str.str("");
    elink_i_str << "board_GPIO_dout_mask";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().board_GPIO_dout_mask.at(i));

    elink_i_str.str("");
    elink_i_str << "board_aligned_TTC";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().m_aligned_TTC_starting_bits.at(i));

    elink_i_str.str("");
    elink_i_str << "board_elink_locked_mask";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().elink_locked_mask.at(i));

    elink_i_str.str("");
    elink_i_str << "board_TTC_phase";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().board_TTC_phase.at(i));

    elink_i_str.str("");
    elink_i_str << "board_ROC_configed_mask";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().board_ROC_configed_mask.at(i));

    elink_i_str.str("");
    elink_i_str << "board_VMM_configed_mask";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().board_VMM_configed_mask.at(i));

    elink_i_str.str("");
    elink_i_str << "board_TDS_configed_mask";
    out_board.put(elink_i_str.str(), miniDAQ_Global_Settings().board_TDS_configed_mask.at(i));
    
    out_miniDAQ_global.add_child(ss.str(), out_board);

  }

  //------------------------------------------------------//
  //                  TTC settings
  //------------------------------------------------------//

  out_miniDAQ_ttc.put("TTC_start_bit",       miniDAQ_TTC_Settings().TTC_start_bit);
  out_miniDAQ_ttc.put("TTC_pulse_lv_val",    miniDAQ_TTC_Settings().TTC_pulse_lv_val);
  out_miniDAQ_ttc.put("Gate_nBC",            miniDAQ_TTC_Settings().Gate_nBC);
  out_miniDAQ_ttc.put("L0_Latency_nBC",      miniDAQ_TTC_Settings().L0_Latency_nBC);
  out_miniDAQ_ttc.put("L1_Latency_nBC",      miniDAQ_TTC_Settings().L1_Latency_nBC);
  out_miniDAQ_ttc.put("L1_Accept_val",       miniDAQ_TTC_Settings().L1_Accept_val);
  out_miniDAQ_ttc.put("BC_Reset_val",        miniDAQ_TTC_Settings().BC_Reset_val);
  out_miniDAQ_ttc.put("Orbit_Reset_val",     miniDAQ_TTC_Settings().Orbit_Reset_val);
  out_miniDAQ_ttc.put("VMM_Soft_Reset_val",  miniDAQ_TTC_Settings().VMM_Soft_Reset_val);
  out_miniDAQ_ttc.put("L0_Accept_val",       miniDAQ_TTC_Settings().L0_Accept_val);
  out_miniDAQ_ttc.put("VMM_TP_val",          miniDAQ_TTC_Settings().VMM_TP_val);
  out_miniDAQ_ttc.put("SCA_Reset_val",       miniDAQ_TTC_Settings().SCA_Reset_val);
  out_miniDAQ_ttc.put("EVID_Reset_val",      miniDAQ_TTC_Settings().EVID_Reset_val);
  out_miniDAQ_ttc.put("L0_Evt_Reset_val",    miniDAQ_TTC_Settings().L0_Evt_Reset_val);
  out_miniDAQ_ttc.put("CKTP_NMax",           miniDAQ_TTC_Settings().CKTP_NMax);
  out_miniDAQ_ttc.put("CKTP_Spacing",        miniDAQ_TTC_Settings().CKTP_Spacing);
  out_miniDAQ_ttc.put("Ext_Trigger_En",      miniDAQ_TTC_Settings().Ext_Trigger_En);
  out_miniDAQ_ttc.put("Daq_En",              miniDAQ_TTC_Settings().Daq_En);
  out_miniDAQ_ttc.put("eFIFO_value",         miniDAQ_TTC_Settings().eFIFO_value);
  
  out_root.add_child("miniDAQ_global_settings", out_miniDAQ_global);
  out_root.add_child("miniDAQ_ttc_settings", out_miniDAQ_ttc);

  outpt.add_child("configuration", out_root);
 
  stringstream sx; 
  try {
        #if BOOST_VERSION >= 105800
    write_xml(filename.toStdString(), outpt, std::locale(),
              boost::property_tree::xml_writer_make_settings<std::string>('\t',1));
        #else
    write_xml(filename.toStdString(), outpt, std::locale(),
              boost::property_tree::xml_writer_make_settings<char>('\t',1));
        #endif

    
    sx.str("");
    sx << "Configuration written successfully to file: \n";
    sx << " > " << filename.toStdString();
    msg()(sx,"ConfigHandlerMiniDAQ::WriteConfig");
    
  }
  catch(std::exception& e) {
    sx.str("");
    sx << "ERROR Unable to write output configuration XML file: " << e.what();
    msg()(sx,"ConfigHandlerMiniDAQ::WriteConfig");
    return;
  }


}
int ConfigHandlerMiniDAQ::AllBoards_TTCAligned() {

  int current_TTC_aligned_bit = -1;
  bool all_board_TTC_aligned_the_same = true;

  for ( uint i=0; i<miniDAQ_Global_Settings().m_aligned_TTC_starting_bits.size(); i++ ) {
    int iboard_TTC_aligned_bit = miniDAQ_Global_Settings().m_aligned_TTC_starting_bits.at(i);
    if ( iboard_TTC_aligned_bit >= 0 ) {

      if ( current_TTC_aligned_bit == -1 ) {
	current_TTC_aligned_bit = iboard_TTC_aligned_bit;
      }
      else if ( current_TTC_aligned_bit == iboard_TTC_aligned_bit ) {
      }
      else { // if current TTC alignment not the same then multiple alignment
	all_board_TTC_aligned_the_same = false;
      }
      
    }
    else {
      continue;
    }
  }

  // if all alignement is the same return the TTC aligned bit;
  // if no TTC alignment is found for any boards then the current_TTC_aligned_bit is -1
  // if found multiple aligned its between different boards return -2;
  if ( all_board_TTC_aligned_the_same ) {
    return current_TTC_aligned_bit;
  }
  else {
    return -2;
  }

}
void ConfigHandlerMiniDAQ::cannotDetectBoard(uint32_t i){

  miniDAQ_Global_Settings().m_aligned_TTC_starting_bits.at(i) = -1;
  miniDAQ_Global_Settings().board_SCA_IDs.at(i)               = 0xFFFFFFFF;
  miniDAQ_Global_Settings().board_is_pFEB.at(i)               = false; 
  miniDAQ_Global_Settings().board_GPIO_dir_mask.at(i)         = 0;
  miniDAQ_Global_Settings().board_GPIO_dout_mask.at(i)        = 0;
  miniDAQ_Global_Settings().elink_locked_mask.at(i)           = 0;

  if ( miniDAQ_Global_Settings().board_exist_mask & ( 0b1 << i ) ) {
    miniDAQ_Global_Settings().board_exist_mask -= ( 0b1 << i );
  }
  if ( miniDAQ_Global_Settings().configured_board_mask & ( 0b1 << i ) ) {
    miniDAQ_Global_Settings().configured_board_mask -= ( 0b1 << i );
  }

  emit new_global_config();

}


//-----------------------------------------------------------------------//                                                
//   Make sure all Channels are Locked                                                                                     
//   Keep Searchig until all links have been locked and then                                                               
//   either unlocked.  This way we know all avaible settings for a lock                                                    
//-----------------------------------------------------------------------//                                                
std::vector<std::vector<int>> ConfigHandlerMiniDAQ::CheckLinkLocked( std::vector< std::vector< std::vector<bool> >> LinkOffsets, 
								     int & nlinks )
{

  // <phase, board, elink> LinkOffsets                                                                                     

  stringstream m_sx;

  std::vector<bool> LinkLocked;
  LinkLocked.resize(0);

  std::vector<std::vector<std::vector<uint>>> LinkLockedLocation;

  for ( uint j=0; j< LinkOffsets.at(0).size(); j++ ) {
    LinkLocked.push_back(false);
  }

  //-----------------------------------------------------------//                                                          
  // "j" is the number of links                                                                                            
  // "i" is the number of offset checked                                                                                   
  //-----------------------------------------------------------//                                                          

  m_sx.str("");
  m_sx << "// ----------------------------------------------------------------------------------------------//";
  msg() ( m_sx );

  m_sx.str("");
  m_sx << "ELink Phase Matrix  ";
  msg() ( m_sx );

  //-------------------------------------//                                                                                
  //         loop over j links                                                                                             
  //-------------------------------------//                                                                                
  for ( uint j=0; j<LinkOffsets.at(0).size(); j++ ) {

    std::vector< std::vector<uint> > LinkLockedLocation_boardj;
    LinkLockedLocation_boardj.resize(0);


    for ( uint k=0; k<LinkOffsets.at(0).at(0).size(); k++ ) {

      m_sx.str("");
      m_sx << "board " << j << " elink " << k << " phases locked: ";


      int n_blocks = 0;
      bool previous_phase_locked = false;

      std::vector<uint> currentLinkLocked;
      std::vector<uint> longestLinkLocked;
      currentLinkLocked.resize(0);
      longestLinkLocked.resize(0);

      //------------------------------------------//                                                                       
      //         loop over first 32 phases                                                                                 
      //------------------------------------------//                                                                       
      for ( uint i=0; i < 32; i++ ) {

        if ( LinkOffsets.size() <= i ) continue;

        if ( LinkOffsets.at(i).at(j).at(k) ) {
          m_sx << 1 << " " ;

          currentLinkLocked.push_back(i);

          // if its last phase and it is locked then consider this a block                                                 
          if ( i == 31 ) {
            if ( currentLinkLocked.size() >= longestLinkLocked.size() ) {
              longestLinkLocked = currentLinkLocked;
              currentLinkLocked.resize(0);
            }
            else {
              currentLinkLocked.resize(0);
            }
          }

          // if the previous phase is not locked then this is the start of a new block of locked phases                    
          if ( !previous_phase_locked ) {
            n_blocks++;
          }

          previous_phase_locked = true;

        }
        else {
          m_sx << 0 << " ";

          //---------------------------------------//                                                                      
          //  end of new patch of locked phases                                                                            
          //---------------------------------------//                                                                      

          if ( currentLinkLocked.size() >= longestLinkLocked.size() ) {
            longestLinkLocked = currentLinkLocked;
            currentLinkLocked.resize(0);
          }
          else {
            currentLinkLocked.resize(0);
          }

          previous_phase_locked = false;
        }

      } // end of loop over 32 phases      

      //-----------------------------------//                                                                              
      //     loop over last 32 phases                                                                                      
      //-----------------------------------//                                                                              

      for ( uint i=32; i < LinkOffsets.size(); i++ ) {

        if ( LinkOffsets.at(i).at(j).at(k) ) {
          m_sx << 1 << " " ;

          currentLinkLocked.push_back(i);

          // if its last phase and it is locked then consider this a block                                                 
          if ( i == LinkOffsets.size() - 1 ) {
            if ( currentLinkLocked.size() >= longestLinkLocked.size() ) {
              longestLinkLocked = currentLinkLocked;
              currentLinkLocked.resize(0);
            }
            else {
              currentLinkLocked.resize(0);
            }
          }

          // if the previous phase is not locked then this is the start of a new block of locked phases                    
          if ( !previous_phase_locked ) {
            n_blocks++;
          }

          previous_phase_locked = true;

        }
        else {
          m_sx << 0 << " ";

          //---------------------------------------//                                                                      
          //  end of new patch of locked phases                                                                            
          //---------------------------------------//                                                                      
          if ( currentLinkLocked.size() >= longestLinkLocked.size() ) {
            longestLinkLocked = currentLinkLocked;
            currentLinkLocked.resize(0);
          }
          else {
            currentLinkLocked.resize(0);
          }

          previous_phase_locked = false;
	}

      } // end of loop over last 32 phases                                                                                 

      //------------------------------------------------------//                                                           

      // push back longest link locked segment, one for each k link on board j                                             
      LinkLockedLocation_boardj.push_back( longestLinkLocked );
      // output line in phase matrix                                                                                       
      msg()(m_sx);

    } // end of loop over k links                                                                                          


    LinkLockedLocation.push_back( LinkLockedLocation_boardj );
  } // end of loop over j boards 

  m_sx.str("");
  m_sx << "// ---------------------------------------------------------------------------------------------- //";
  msg() ( m_sx );

  //------------------------------------------------------------------------------//                                       
  //     Pick out the best phase from the longest continous locked phase block                                             
  //------------------------------------------------------------------------------//                                       

  std::vector<std::vector<int>> Elink_bestLockLoc;
  Elink_bestLockLoc.resize(0);
  nlinks = 0;

  m_sx.str("");
  m_sx << " Best Elink phases: ";

  for ( uint j=0; j<LinkLockedLocation.size(); j++ ) {

    std::vector< int > Elink_bestLockLoc_boardj;
    Elink_bestLockLoc_boardj.resize(0);

    for ( uint k=0; k<LinkLockedLocation.at(j).size(); k++ ) {

      std::vector<uint> linkLoc_j_k = LinkLockedLocation.at(j).at(k);

      // eLink j is unlocked                                                                                               
      if ( linkLoc_j_k.size() == 0 ) {
        Elink_bestLockLoc_boardj.push_back(-1);
        m_sx << "NA ";
      }
      else {
        Elink_bestLockLoc_boardj.push_back( linkLoc_j_k.at( linkLoc_j_k.size()/2 ) ); // take the middle index             
        nlinks++;
        m_sx << linkLoc_j_k.at( linkLoc_j_k.size()/2 ) << " ";
      }
    }

    Elink_bestLockLoc.push_back( Elink_bestLockLoc_boardj );

  }

  m_sx << "\n";
  msg()(m_sx);

  return Elink_bestLockLoc;
}

// -------------------------------------------------------------------------- //
MiniDAQTTCSetting ConfigHandlerMiniDAQ::LoadMiniDAQTTCSettings( const boost::property_tree::ptree& pt)
{

  using boost::property_tree::ptree;
  using boost::format;

  MiniDAQTTCSetting t;

  bool outok = true;
  bool found = false;

  try{

    for(const auto& conf : pt.get_child("configuration")){

      if(!(conf.first == "miniDAQ_ttc_settings")) continue;
      t.TTC_start_bit    = conf.second.get<uint32_t>("TTC_start_bit");
      t.TTC_pulse_lv_val = conf.second.get<uint32_t>("TTC_pulse_lv_val");
      t.Gate_nBC         = conf.second.get<uint32_t>("Gate_nBC");
      t.L0_Latency_nBC   = conf.second.get<uint32_t>("L0_Latency_nBC");
      t.L1_Latency_nBC   = conf.second.get<uint32_t>("L1_Latency_nBC");

      t.L1_Accept_val      = conf.second.get<int>("L1_Accept_val");
      t.BC_Reset_val       = conf.second.get<int>("BC_Reset_val");
      t.Orbit_Reset_val    = conf.second.get<int>("Orbit_Reset_val");
      t.VMM_Soft_Reset_val = conf.second.get<int>("VMM_Soft_Reset_val");
      t.L0_Accept_val      = conf.second.get<int>("L0_Accept_val");
      t.VMM_TP_val         = conf.second.get<int>("VMM_TP_val");
      t.SCA_Reset_val      = conf.second.get<int>("SCA_Reset_val");
      t.EVID_Reset_val     = conf.second.get<int>("EVID_Reset_val");
      t.L0_Evt_Reset_val   = conf.second.get<int>("L0_Evt_Reset_val");

      t.CKTP_NMax          = conf.second.get<uint32_t>("CKTP_NMax");
      t.CKTP_Spacing       = conf.second.get<uint32_t>("CKTP_Spacing");

      t.Ext_Trigger_En     = conf.second.get<int>("Ext_Trigger_En");
      t.Daq_En             = conf.second.get<int>("Daq_En");
 
      t.eFIFO_value        = conf.second.get<uint32_t>("eFIFO_value");

      found = true;
    }
   
  }

  catch(std::exception &e){
      stringstream sx;
      sx << "Exception in loading MiniDAQ ttc registers: " << e.what();
      msg()(sx,"ConfigHandlerMiniDAQ::LoadMiniDAQTTCSettings");
      outok = false;
  }

  if ( !found ) outok = false;
  t.ok = outok;

  m_ttc_miniDAQ = t;

  emit new_ttc_config();

  return t;

}
 
MiniDAQGlobalSetting ConfigHandlerMiniDAQ::LoadMiniDAQGlobalSettings( const boost::property_tree::ptree& pt)
{
   
  using boost::property_tree::ptree;
  using boost::format;

  stringstream sx;
  stringstream ss;
  
  MiniDAQGlobalSetting g;

  bool outok = true;
  bool found = false;

  try{

     for(const auto& conf : pt.get_child("configuration")){

       if(!(conf.first == "miniDAQ_global_settings")) continue;
       
       g.board_exist_mask = conf.second.get<uint32_t>("board_exist_mask");
       g.configured_board_mask = conf.second.get<int>("configured_board_mask");

       found = true;
     }
  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading MiniDAQ global registers: " << e.what();
    msg()(sx,"ConfigHandlerMiniDAQ::LoadMiniDAQGlobalSettings");
    outok = false;
  } 

  if ( !found ) outok = false;

  try{

     for(int iBoard = 0; iBoard < 8; iBoard++) {
    
        ss.str("");
        ss << "board_" << format("%01i") % iBoard;
        for(const auto& conf : pt.get_child("configuration.miniDAQ_global_settings")) {
     
           size_t find = conf.first.find(ss.str());
           if(find==string::npos) continue;

           uint32_t SCA_ID           = conf.second.get<uint32_t>("board_SCA_ID");
	   bool     is_pFEB          = conf.second.get<bool>("board_is_pFEB");
           uint32_t GPIO_dir_mask    = conf.second.get<uint32_t>("board_GPIO_dir_mask");
           uint32_t GPIO_dout_mask   = conf.second.get<uint32_t>("board_GPIO_dout_mask");
	   int      TTC_Bit          = conf.second.get<int>("board_aligned_TTC");
	   uint32_t board_elink_mask = conf.second.get<uint32_t>("board_elink_locked_mask");
	   uint32_t TTC_phase        = conf.second.get<uint32_t>("board_TTC_phase");

	   uint32_t board_ROC_configed_mask_i = conf.second.get<uint32_t>("board_ROC_configed_mask");
           uint32_t board_VMM_configed_mask_i = conf.second.get<uint32_t>("board_VMM_configed_mask");
           uint32_t board_TDS_configed_mask_i = conf.second.get<uint32_t>("board_TDS_configed_mask");

           g.board_SCA_IDs.at(iBoard)        = SCA_ID;
	   g.board_is_pFEB.at(iBoard)        = is_pFEB;
           g.board_GPIO_dir_mask.at(iBoard)  = GPIO_dir_mask;
           g.board_GPIO_dout_mask.at(iBoard) = GPIO_dout_mask;

	   g.m_aligned_TTC_starting_bits.at(iBoard) = TTC_Bit;
	   g.elink_locked_mask.at(iBoard)           = board_elink_mask;
	   g.board_TTC_phase.at(iBoard)             = TTC_phase;

	   g.board_ROC_configed_mask.at(iBoard)     = board_ROC_configed_mask_i;
           g.board_VMM_configed_mask.at(iBoard)     = board_VMM_configed_mask_i;
           g.board_TDS_configed_mask.at(iBoard)     = board_TDS_configed_mask_i;

        }

     }

  }
  catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading MiniDAQ global registers: " << e.what();
    msg()(sx,"ConfigHandlerMiniDAQ::LoadMiniDAQGlobalSettings");
    outok = false;
   }
  
   try{

     for(const auto& conf : pt.get_child("configuration.miniDAQ_global_settings")) {

       size_t find = conf.first.find("ElinkMatrix");
       if(find==string::npos) continue;

       // < phase, board, elink >                                                                                                              
       for ( uint i=0; i<64; i++ ) {
	 
	 ss.str("");
	 ss << "Phase_" << format("%02i") % i << "_elink_mask";
	 
	 uint32_t elink_locked_mask = conf.second.get<uint32_t>(ss.str());
	 
	 // j = board, k = elink on board                                                                                                      
	 for ( uint j=0; j<8; j++ ) {
	   for ( uint k=0; k<4; k++ ) {
	     
	     if ( (elink_locked_mask & (0b1 << (4*j+k))) ) {
	       g.elink_matrix.at(i).at(j).at(k) = true;
	     }
	     else {
	       g.elink_matrix.at(i).at(j).at(k) = false;
	     }
	     
	   }
	 }
	 
       }

     }
   }
   catch(std::exception &e){
    stringstream sx;
    sx << "Exception in loading MiniDAQ global registers: " << e.what();
    msg()(sx,"ConfigHandlerMiniDAQ::LoadMiniDAQGlobalSettings");
    outok = false;
   }

  g.ok = outok;
 
  m_global_miniDAQ = g;

  emit new_global_config();

  return g;
  
}

//// ------------------------------------------------------------------------ //
void ConfigHandlerMiniDAQ::LoadMiniDAQConfiguration(MiniDAQGlobalSetting &global, MiniDAQTTCSetting &ttc)
{
  m_global_miniDAQ = global;
  m_ttc_miniDAQ = ttc;

  emit new_ttc_config();
  emit new_global_config();
}

void ConfigHandlerMiniDAQ::LoadMiniDAQConfigurationGlobal(MiniDAQGlobalSetting &global)
{
  m_global_miniDAQ = global;
  emit new_global_config();
}
void ConfigHandlerMiniDAQ::LoadMiniDAQConfigurationTTC(   MiniDAQTTCSetting &ttc)
{
  m_ttc_miniDAQ = ttc;
  emit new_ttc_config();
}


//-------------------------------------------------------//
bool ConfigHandlerMiniDAQ::AllBoardElinksDetected() {

  bool AllElinkDetected = true;

  uint32_t elink_mask = miniDAQ_Global_Settings().ComputeElinkMask();

  for ( uint i=0; i<8; i++ ) {

    // if board i exists
    if ( miniDAQ_Global_Settings().board_exist_mask & ( 0b1 << i ) ) {
      
      for ( uint j=0; j<4; j++ ) {
	
        if ( elink_mask & (0b1 << (i*4+j)) ) {
	  // link j on board i is locked 
        }
        else {
	  AllElinkDetected = false;
        }
      }

    }
  }

  return AllElinkDetected;

}
//-------------------------//
int ConfigHandlerMiniDAQ::nElinks() {

  int nlinks=0;

  for ( int i=0; i<8; i++ ) {
    if ( miniDAQ_Global_Settings().ComputeElinkMask() & ( 0b1 << i ) ) nlinks++;
  }

  return nlinks;
}
//--------------------------//
int ConfigHandlerMiniDAQ::TargetPort(){
  return miniDAQ_Global_Settings().target_port;
}
QHostAddress ConfigHandlerMiniDAQ::TargetIp(){
  QHostAddress TargetIp;
  TargetIp.setAddress( miniDAQ_Global_Settings().target_ip );
  return TargetIp;
}
//--------------------------//
void ConfigHandlerMiniDAQ::clear_miniDAQ_global() {
  miniDAQ_Global_Settings().clear();

  emit new_global_config();
}
void ConfigHandlerMiniDAQ::clear_miniDAQ_ttc() {

  miniDAQ_TTC_Settings().clear();

  emit new_ttc_config();
}
