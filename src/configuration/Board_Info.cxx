#include "Board_Info.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


Board_Info::Board_Info() 
{

  SCA_ID = 0xFFFFFFFF;
  is_pFEB = false;
  
  VMMs_Configured = new vector<bool>;
  v_boardsChecked = new vector<bool>;
  max_binContent  = new vector<int>;
  hist_Found      = new vector<int>;

  v_dead_channels    = new std::vector< std::vector<int>* >;
  v_lowpdoChannels   = new std::vector< std::vector<std::pair<int,double>>* >;
  v_highpdoChannels  = new std::vector< std::vector<std::pair<int,double>>* >;
  v_lessHitsChannels = new std::vector< std::vector<std::pair<int,double>>* >;
  
  Clear();

}

Board_Info::~Board_Info() {

  delete VMMs_Configured;
  delete v_boardsChecked;
  delete max_binContent;
  delete hist_Found;

  delete v_dead_channels;
  delete v_lowpdoChannels;
  delete v_highpdoChannels;
  delete v_lessHitsChannels;
}

void Board_Info::Clear() {

  SCA_ID = 0xFFFFFFFF;
  is_pFEB = false;

  VMMs_Configured->resize(0);
  v_boardsChecked->resize(0);
  max_binContent->resize(0);
  hist_Found->resize(0);
  
  v_dead_channels->resize(0);
  v_lowpdoChannels->resize(0);
  v_highpdoChannels->resize(0);
  v_lessHitsChannels->resize(0);

  std::vector<int> * null_vec_int = new std::vector<int>;
  null_vec_int->resize(0);

  std::vector<std::pair<int,double>> * null_vec_pair1 = new std::vector<std::pair<int,double>>;
  std::vector<std::pair<int,double>> * null_vec_pair2 = new std::vector<std::pair<int,double>>;
  std::vector<std::pair<int,double>> * null_vec_pair3 = new std::vector<std::pair<int,double>>;
  null_vec_pair1->resize(0);
  null_vec_pair2->resize(0);
  null_vec_pair3->resize(0);
  
  for ( uint ivmm = 0; ivmm<8; ivmm++ ) {
    
    VMMs_Configured->push_back(false);
    v_boardsChecked->push_back(false);
    max_binContent->push_back(0);
    hist_Found->push_back(false);

    v_dead_channels->push_back(null_vec_int);
    v_lowpdoChannels->push_back(null_vec_pair1);
    v_highpdoChannels->push_back(null_vec_pair2);
    v_lessHitsChannels->push_back(null_vec_pair3);
  }

}

//------------------------------------------------------------------//
//               Add new information in a sorted manner
//------------------------------------------------------------------//

void Board_Info::Sorted_Insert( std::vector<int> * vec, int val ) {

  for ( auto ith = vec->begin(); ith < vec->end(); ith++ ) {
    if ( *ith == val ) {
      break; //already exits don't insert
    }
    else if ( *ith > val ) {
      vec->insert(ith, val); // insert before a larger value
    }
    else if ( ith+1 == (vec->end()) ) {
      vec->push_back(val); // at the end, did not find ivmm, and ivmm bigger then all. Put ivmm at the end.
      break;
    }
  }
 
}

void Board_Info::Sorted_Insert( std::vector<uint32_t> * vec, uint32_t val ) {

  for ( auto ith = vec->begin(); ith != vec->end(); ith++ ) {
    if ( *ith == val ) {
      break; //already exits don't insert
    }
    else if ( *ith > val ) {
      vec->insert(ith, val); // insert before a larger value
    }
    else if ( ith+1 == (vec->end()) ) {
      vec->push_back(val); // at the end, did not find ivmm, and ivmm bigger then all. Put ivmm at the end.
      break;
    }
  }
  
}

void Board_Info::Sorted_Insert( std::vector< std::pair<int,double> > * vec, std::pair<int,double> val ) {

  for ( auto ith = vec->begin(); ith != vec->end(); ith++ ) {
    if ( (*ith).first == val.first ) {
      break; //already exits don't insert
    }
    else if ( (*ith).first > val.first ) {
      vec->insert(ith, val); // insert before a larger value
    }
    else if ( ith+1 == (vec->end()) ) {
      vec->push_back(val); // at the end, did not find ivmm, and ivmm bigger then all. Put ivmm at the end.
      break;
    }
  }

}

//------------------------------------------------------------//
//      Add Each Type of Information by using Sorted_Insert
//------------------------------------------------------------//

bool Board_Info::Add_New_Configed_VMM( uint ivmm ) {
  if ( VMMs_Configured->size() <= ivmm ) {
    std::cout << "Error: VMMs_Configured size " << VMMs_Configured->size()
	      << " is smaller than " << ivmm << std::endl;
    return false;
  }
  else {
    VMMs_Configured->at(ivmm) = true;
  }

  return true;
}

bool Board_Info::Add_New_Found_VMM_Histo( uint ivmm ) {

  if ( hist_Found->size() <= ivmm ) {
    std::cout << "Error: hist_Found size " << hist_Found->size()
	      << " is smaller than " << ivmm << std::endl;
    return false;
  }
  else {
    hist_Found->at(ivmm) = true;
  }

  return true;
}

bool Board_Info::Add_New_Dead_Channel( uint ivmm, int ichan ) {

  if ( v_dead_channels->size() < ivmm ) {
    std::cout << "Error: v_dead_channels size " << v_dead_channels->size()
	      << " is smaller than " << ivmm << std::endl;
    return false;
  }
  else {
    //  Insert the dead channel in the list of dead channels
    Sorted_Insert( v_dead_channels->at(ivmm), ichan );
  }

  return true;
  
}

bool Board_Info::Add_New_Ineff_Channel( uint ivmm, int ichan, double eff ) {

  std::pair<int,double> ichan_eff = std::make_pair(ichan, eff);
  
  if ( v_lessHitsChannels->size() < ivmm ) {
    std::cout << "Error: v_lessHitsChannels size " << v_lessHitsChannels->size()
	      << " is smaller than " << ivmm << std::endl;
    return false;
  }
  else {
    //  Insert the inefficient channel in the list of inefficient channels
    Sorted_Insert( v_lessHitsChannels->at(ivmm), ichan_eff );
  }

  return true;
  
}

bool Board_Info::Add_New_LowPDOChannel( uint ivmm, int ichan, double i_avg_pdo ) {

  std::pair<int,double> ichan_pdo = std::make_pair(ichan, i_avg_pdo);

  if ( v_lowpdoChannels->size() < ivmm ) {
    std::cout << "Error: v_lowpdoChannels size " << v_lowpdoChannels->size()
	      << " is smaller than " << ivmm << std::endl;
    return false;
  }
  else {
    //  Insert the low pdo channel in the list of low pdo channels
    Sorted_Insert( v_lowpdoChannels->at(ivmm), ichan_pdo );
  }

  return true;
}

bool Board_Info::Add_New_HighPDOChannel( uint ivmm, int ichan, double i_avg_pdo ) {

  std::pair<int,double> ichan_pdo = std::make_pair(ichan, i_avg_pdo);

  if ( v_highpdoChannels->size() < ivmm ) {
    std::cout << "Error: v_highpdoChannels size " << v_highpdoChannels->size()
	      << " is smaller than " << ivmm << std::endl;
    return false;
  }
  else {
    //  Insert the high pdo channel in the list of high pdo channels
    Sorted_Insert( v_highpdoChannels->at(ivmm), ichan_pdo );
  }

  return true;
  
}

//---------------------------------------------------------------------//
//     Merge another board information into this board information
//---------------------------------------------------------------------//

bool Board_Info::UploadInfo(Board_Info * new_info) {

  if ( new_info->SCA_ID != SCA_ID || SCA_ID == 0xFFFFFFFF) {
    std::cout << "Error: Attempting to merge information for two boards with different SCA_IDs: "
	      << new_info->SCA_ID << " into " << SCA_ID << std::endl;
    return false;
  }

  //-----------------------------------------------------------------//
  //              If VMM is configured in new information
  //              add it to the list of configured vmms
  //-----------------------------------------------------------------//

  std::cout << "adding configed vmm " << std::endl;
  
  for ( uint ivmm=0; ivmm < new_info->VMMs_Configured->size(); ivmm++ ) {
    if ( new_info->VMMs_Configured->at(ivmm) ) {
      Add_New_Configed_VMM( ivmm );
    }
  }
  
  //---------------------------------------------------------------------------------//
  //     If Hist for iVMM is found in new Info, Set Hist Found for ivmm to be true
  //---------------------------------------------------------------------------------//

  std::cout << "adding hist found " << std::endl;
  
  for ( uint ivmm=0; ivmm < new_info->hist_Found->size(); ivmm++ ) {
    if ( new_info->hist_Found->at(ivmm) ) {
      Add_New_Found_VMM_Histo( ivmm );
    }
  }
  
  //---------------------------------------------------------------------------------//
  //              Merge new dead channels found in v_dead_channels
  //---------------------------------------------------------------------------------//

  std::cout << "adding dead channels " << std::endl;
  
  for ( uint ivmm=0; ivmm < new_info->v_dead_channels->size(); ivmm++ ) {

    for ( auto ich = new_info->v_dead_channels->at(ivmm)->begin();
	  ich < new_info->v_dead_channels->at(ivmm)->end(); ich++ ) {
      
      Add_New_Dead_Channel(ivmm, (*ich)); 
    }    
  }

  //---------------------------------------------------------------------------------//
  //              Merge new inefficient channels found in v_lessHitschannels
  //---------------------------------------------------------------------------------//

  std::cout << "adding inefficient channels " << std::endl;
  
  for ( uint ivmm=0; ivmm < new_info->v_lessHitsChannels->size(); ivmm++ ) {

    for ( auto ich = new_info->v_lessHitsChannels->at(ivmm)->begin();
	  ich < new_info->v_lessHitsChannels->at(ivmm)->end(); ich++ ) {

      Add_New_Ineff_Channel(ivmm, (*ich).first, (*ich).second);
    }
  }
  
  //---------------------------------------------------------------------------------//
  //              Merge new low pdo channels found in v_lowpdoChannels
  //---------------------------------------------------------------------------------//

  std::cout << "adding low pdo channel " << std::endl;
  
  for ( uint ivmm=0; ivmm < new_info->v_lowpdoChannels->size(); ivmm++ ) {

    for ( auto ich = new_info->v_lowpdoChannels->at(ivmm)->begin();
	  ich < new_info->v_lowpdoChannels->at(ivmm)->end(); ich++ ) {

      Add_New_LowPDOChannel(ivmm, (*ich).first, (*ich).second);
    }
  }

  //---------------------------------------------------------------------------------//
  //              Merge new high pdo channels found in v_highpdoChannels
  //---------------------------------------------------------------------------------//

  std::cout << "adding high pdo channels " << std::endl;
  
  for ( uint ivmm=0; ivmm < new_info->v_highpdoChannels->size(); ivmm++ ) {

    for ( auto ich = new_info->v_highpdoChannels->at(ivmm)->begin();
	  ich < new_info->v_highpdoChannels->at(ivmm)->end(); ich++ ) {

      Add_New_HighPDOChannel(ivmm, (*ich).first, (*ich).second);
    }
  }

  std::cout << "finished merging " << std::endl;
  
  return true;
  
}


