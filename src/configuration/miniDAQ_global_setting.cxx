#include "miniDAQ_global_setting.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


//////////////////////////////////////////////////////////////////////////////
// ------------------------------------------------------------------------ //
//  GlobalSetting
// ------------------------------------------------------------------------ //
//////////////////////////////////////////////////////////////////////////////
MiniDAQGlobalSetting::MiniDAQGlobalSetting() :
  configured_board_mask(0),
  board_exist_mask(0),
  ok(false)
{
  
  target_ip = "192.168.0.1";
  target_port = 6008;

  elink_locked_mask.resize(0);

  // <phase, board, elink> elink_matrix
  elink_matrix.resize(0);
  board_SCA_IDs.resize(0);
  board_is_pFEB.resize(0);
  board_GPIO_dir_mask.resize(0);
  board_GPIO_dout_mask.resize(0);

  board_ROC_configed_mask.resize(0);
  board_VMM_configed_mask.resize(0);
  board_TDS_configed_mask.resize(0);

  board_current_TDS_register.resize(0);
  board_current_ROC_register.resize(0);

  board_TTC_phase.resize(0);

  m_aligned_TTC_starting_bits.resize(0);

  std::vector<bool> empty;
  std::vector<uint32_t> blank;
  empty.resize(0);
  blank.resize(0);

  for ( int i=0; i<4; i++ ) {
    empty.push_back(false);
  }

  for ( int i=0; i<64; i++ ) {
    std::vector<std::vector<bool>> empty_i;
    empty_i.resize(0);
    for ( int j=0; j<8; j++ ) {
      empty_i.push_back( empty );
    }
    elink_matrix.push_back(empty_i);
  }

  std::vector<std::vector<uint32_t>> blankblank;
  blankblank.clear();
  for ( int j=0; j<4; j++ ) {
    blankblank.push_back(blank);
  }

  for ( int i=0; i<8; i++ ) {
    board_SCA_IDs.push_back( 0xFFFFFFFF );
    board_is_pFEB.push_back( false );

    board_GPIO_dir_mask.push_back(0x0);
    board_GPIO_dout_mask.push_back(0x0);
    m_aligned_TTC_starting_bits.push_back(-1);
    elink_locked_mask.push_back( 0 );
    board_TTC_phase.push_back(0);
    board_ROC_configed_mask.push_back(0);
    board_VMM_configed_mask.push_back(0);
    board_TDS_configed_mask.push_back(0);

    board_current_TDS_register.push_back(blankblank);
    board_current_ROC_register.push_back(blankblank);
  }

}

void MiniDAQGlobalSetting::clear() {

  target_ip = "192.168.0.1";
  target_port = 6008;

  configured_board_mask = 0;
  board_exist_mask = 0;
  ok = false;

  elink_locked_mask.resize(0);

  // <phase, board, elink> elink_matrix
  elink_matrix.resize(0);
  board_SCA_IDs.resize(0);
  board_is_pFEB.resize(0);

  board_GPIO_dir_mask.resize(0);
  board_GPIO_dout_mask.resize(0);
  m_aligned_TTC_starting_bits.resize(0);
  board_TTC_phase.resize(0);

  board_ROC_configed_mask.resize(0);
  board_VMM_configed_mask.resize(0);
  board_TDS_configed_mask.resize(0);

  board_current_TDS_register.resize(0);
  board_current_ROC_register.resize(0);

  std::vector<bool> empty;
  std::vector<uint32_t> blank;
  empty.resize(0);
  blank.resize(0);

  for ( int i=0; i<4; i++ ) {
    empty.push_back(false);
  }

  for ( int i=0; i<64; i++ ) {
    std::vector<std::vector<bool>> empty_i;
    empty_i.resize(0);
    for ( int j=0; j<8; j++ ) {
      empty_i.push_back( empty );
    }
    elink_matrix.push_back(empty_i);
  }

  std::vector<std::vector<uint32_t>> blankblank;
  blankblank.clear();
  for ( int j=0; j<4; j++ ) {
    blankblank.push_back(blank);
  }

  for ( int i=0; i<8; i++ ) {
    board_SCA_IDs.push_back( 0xFFFFFFFF );
    board_is_pFEB.push_back( false );

    board_GPIO_dir_mask.push_back(0x0);
    board_GPIO_dout_mask.push_back(0x0);
    m_aligned_TTC_starting_bits.push_back(-1);
    elink_locked_mask.push_back( 0 );
    board_TTC_phase.push_back( 0 );
    board_ROC_configed_mask.push_back(0);
    board_VMM_configed_mask.push_back(0);
    board_TDS_configed_mask.push_back(0);

    board_current_TDS_register.push_back(blankblank);
    board_current_ROC_register.push_back(blankblank);
  }


}

uint32_t MiniDAQGlobalSetting::ComputeElinkMask() {

  uint32_t mask = 0;

  for ( uint i=0; i < elink_locked_mask.size(); i++ ) {
    mask += ( elink_locked_mask.at(i) << ( 4 * i ) );
  }

  return mask;

}

void MiniDAQGlobalSetting::setElinkLockedVector( std::vector<std::vector<bool>> locked_elinks ) {
  for ( uint i=0; i < locked_elinks.size(); i++ ) {
    uint32_t mask=0;
    for ( uint j=0; j<locked_elinks.at(i).size(); j++ ) {
      mask += ( 0b1 << j );
    }
    if ( i < elink_locked_mask.size() ) {
      elink_locked_mask.at(i) = mask;
    }
    else {
      std::cout << "Error: WTF locked elinks too long for number of boards!!!!!!" << std::endl;
    }
  }
}

void MiniDAQGlobalSetting::print()
{
   /*
    stringstream ss;
    ss << "------------------------------------------------------" << endl;
    ss << " Global Settings " << endl;

    ss << "     > channel polarity          : "
        << polarity << " ("
        << GlobalSetting::all_polarities[polarity].toStdString() << ")" << endl;

    cout << ss.str() << endl;
    */

}
