#include "miniDAQ_ttc_setting.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;


MiniDAQTTCSetting::MiniDAQTTCSetting() :
    TTC_start_bit(0),
    TTC_pulse_lv_val(0),
    L1_Accept_val(0),
    BC_Reset_val(0),
    Orbit_Reset_val(0),
    VMM_Soft_Reset_val(0),
    L0_Accept_val(0),
    VMM_TP_val(0),
    SCA_Reset_val(0),
    EVID_Reset_val(0),
    L0_Evt_Reset_val(0),
    Ext_Trigger_En(0),
    Daq_En(0),
    ok(false)
{
    Gate_nBC = 0;
    L0_Latency_nBC = 0;
    L1_Latency_nBC = 0;

    CKTP_NMax = 0;
    CKTP_Spacing = 0;
    eFIFO_value = 0;
}
void MiniDAQTTCSetting::print()
{

}
void MiniDAQTTCSetting::clear()
{
  TTC_start_bit      = 0;
  TTC_pulse_lv_val   = 0;
  L1_Accept_val      = 0;
  BC_Reset_val       = 0;
  Orbit_Reset_val    = 0;
  VMM_Soft_Reset_val = 0;
  L0_Accept_val      = 0;
  VMM_TP_val         = 0;
  SCA_Reset_val      = 0;
  EVID_Reset_val     = 0;
  L0_Evt_Reset_val   = 0;
  Ext_Trigger_En     = 0;
  Daq_En             = 0;
  ok                 = false;

  Gate_nBC       = 0;
  L0_Latency_nBC = 0;
  L1_Latency_nBC = 0;
  
  CKTP_NMax      = 0;
  CKTP_Spacing   = 0;
  eFIFO_value    = 0;
}
